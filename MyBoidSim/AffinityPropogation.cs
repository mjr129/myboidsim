﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    class AffinityPropogation
    {
        public delegate void ClusterProg(double[,] a, double[,] r);

        public static int[] Cluster(List<Boid> boids, ClusterProg info)
        {
            // Construct similarity matrix
            int N = boids.Count;
            double[,] s = new double[N, N];
            double[,] r = new double[N, N];
            double[,] a = new double[N, N];
            double[,] rn = new double[N, N];
            double[,] an = new double[N, N];

            // CALCULATE SIMILARITIES "S"
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    s[i, j] = 1024 - boids[i].Location.Distance(boids[i].GetPercievedLocation(boids[j]));

                    s[i, j] += Rnd.Random.NextDouble();
                }
            }

            // CALCULATE PREFERENCES "diag(S)"
            //double median = ApMedian(s);

            double median = 512;

            const double lambda = 0.5;

            for (int i = 0; i < N; i++)
            {
                s[i, i] = median;
            }

            // CALCULATE NEXT R AND NEXT A
            for (int iter = 0; iter < 100; iter++)
            {
                info(a, r);

                // CALCULATE R
                // r[i,k] =   s[i,k]
                //          - max( a[i,kp] + s[i,kp] )
                for (int i = 0; i < N; i++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        double v = double.MinValue;

                        for (int kp = 0; kp < N; kp++)
                        {
                            if (kp != k)
                            {
                                v = Math.Max(v, a[i, kp] + s[i, kp]);
                            }
                        }

                        rn[i, k] = s[i, k] - v;
                    }
                }

                for (int i = 0; i < N; i++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        r[i, k] = r[i, k] * lambda + rn[i, k] * (1 - lambda);
                    }
                }

                // CALCULATE A
                // a[i,k] = min(0, r(k,k)
                //                 + sum( max ( 0, 
                //                        r(ip, k)
                // a[k, k] = sum( max( 0, r(ip, k ) )
                for (int i = 0; i < N; i++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        if (i != k)
                        {
                            double v = 0;

                            for (int ip = 0; ip < N; ip++)
                            {
                                if (ip != i && ip != k)
                                {
                                    v += Math.Max(0, r[ip, k]);
                                }
                            }

                            an[i, k] = Math.Min(0, r[k, k] + v);
                        }
                        else
                        {
                            double v = 0;

                            for (int ip = 0; ip < N; ip++)
                            {
                                if (ip != i && ip != k)
                                {
                                    v += Math.Max(0, r[ip, k]);
                                }
                            }

                            an[k, k] = v;
                        }
                    }
                }

                for (int i = 0; i < N; i++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        a[i, k] = a[i, k] * lambda + an[i, k] * (1 - lambda);
                    }
                }
            }

            // CALCULATE EXEMPLARS "E"
            int[] e = new int[N];

            for (int i = 0; i < N; i++)
            {
                double maxVal = double.MinValue;
                int maxK = -1;

                for (int k = 0; k < N; k++)
                {
                    double val = a[i, k] + r[i, k];

                    if (val > maxVal)
                    {
                        maxVal = val;
                        maxK = k;
                    }
                }

                e[i] = maxK;
            }

            return e;
        }

        /// <summary>
        /// Calculate the median of matrix "S".
        /// Exclude the diagonal from the calculation.
        /// </summary>
        private static double ApMedian(double[,] s)
        {
            // Flatten the matrix and remove the diagnonal
            double[] flat = new double[s.Length - s.GetLength(0)];
            int k = 0;

            for (int i = 0; i < s.GetLength(0); i++)
            {
                for (int j = 0; j < s.GetLength(1); j++)
                {
                    if (i != j)
                    {
                        flat[k] = s[i, j];
                        k++;
                    }
                }
            }

            System.Diagnostics.Debug.Assert(k == flat.Length);

            Array.Sort(flat);
            return flat[flat.Length / 2];
        }
    }
}
