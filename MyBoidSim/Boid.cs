﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBoidSim
{
    class DebugFriend
    {
        public Boid Boid;
        public Vector PercievedLocation;
        public int Zone;

        public DebugFriend(Boid other, Vector otherLoc, int p)
        {
            // TODO: Complete member initialization
            this.Boid = other;
            this.PercievedLocation = otherLoc;
            this.Zone = p;
        }
    }

    enum InformedStatus
    {
        All = -1,
        Naive,
        Alpha,
        Beta,
    }

    class Boid
    {
        public Vector Location;
        public double Angle;
        private BoidSet Owner;
        public Dictionary<int, DebugFriend> DebugFriends = new Dictionary<int, DebugFriend>();
        public bool DebugBoid;
        private readonly int Uid;
        static int UidC;
        private double RandomAngle;
        public int index;

        public InformedStatus Informed
        {
            get
            {
                if (Owner.Constants.InformedMethod == InformedMethod.None)
                {
                    return InformedStatus.Naive;
                }
                else if (index < Owner.Constants.InformedRate)
                {
                    return InformedStatus.Alpha;
                }
                else if (Owner.Constants.SplitInformation && index < Owner.Constants.InformedRate * 2)
                {
                    return InformedStatus.Beta;
                }

                return InformedStatus.Naive;
            }
        }

        public Boid(BoidSet boidSet, int index)
        {
            this.Owner = boidSet;
            this.index = index;
            Uid = ++UidC;
            RandomAngle = (Rnd.Random.NextDouble() * Math.PI * 2) - Math.PI;
            Reset();
        }

        internal string GetInfo()
        {
            return "Location = " + Location.ToString();
        }

        public void TurnTowards(double angleToFace, double magnitude)
        {
            Angle = TurnAngle(Angle, angleToFace, magnitude);
        }

        public void TurnAwayFrom(Vector point, double magnitude)
        {
            double angleTo = Location.AngleTo(point);

            TurnTowards(angleTo + Math.PI, magnitude);
        }

        internal void TurnTowards(Vector point, double magnitude)
        {
            double angleTo = Location.AngleTo(point);

            TurnTowards(angleTo, magnitude);
        }

        public static double TurnAngle(double initial, double target, double delta)
        {
            double d = Difference(initial, target);

            if (delta > Math.Abs(d))
            {
                delta = Math.Abs(d);
            }

            switch (Math.Sign(d))
            {
                case 1:
                    initial += delta;
                    break;

                case -1:
                    initial -= delta;
                    break;
            }

            initial = WrapAngle(initial);

            return initial;
        }

        public static double WrapAngle(double angle)
        {
            while (angle > Math.PI)
            {
                angle -= 2 * Math.PI;
            }
            while (angle < -Math.PI)
            {
                angle += 2 * Math.PI;
            }

            return angle;
        }

        public static double Difference(double a, double b)
        {
            // BROKEN
            a = WrapAngle(a);
            b = WrapAngle(b);

            double r = b - a;

            if (r > Math.PI)
            {
                r = -(Math.PI * 2) + r;
            }
            else if (r < -Math.PI)
            {
                r = (Math.PI * 2) + r;
            }

            return r;
        }

        internal void Process()
        {
            Vector turnTowardsAverage = new Vector();
            Vector turnAwayFromAverage = new Vector();
            Vector alignWithAverage = new Vector();
            int turnTowardsCount = 0;
            int turnAwayFromCount = 0;
            int alignWithCount = 0;

            if (DebugBoid)
            {
                DebugFriends.Clear();
            }

            foreach (Boid other in Owner.Boids)
            {
                if (other == this)
                {
                    continue;
                }

                Vector otherLoc = GetPercievedLocation(other);

                double angle = Location.AngleTo(otherLoc);
                double angleBetween = Math.Abs(Difference(Angle, angle));
                bool canSee = (Owner.Constants.ViewAngle == Math.PI) || angleBetween < Owner.Constants.ViewAngle;

                if (canSee)
                {
                    double dist = otherLoc.DistanceSquared(Location);

                    if (dist < Owner.Constants.TurnAwayFromZone)
                    {
                        turnAwayFromAverage += otherLoc;
                        turnAwayFromCount++;

                        if (DebugBoid)
                        {
                            DebugFriends[other.Uid] = new DebugFriend(other, otherLoc, 3);
                        }
                    }

                    if (dist < Owner.Constants.AlignWithZone)
                    {
                        alignWithAverage += new Vector(other.Angle);
                        alignWithCount++;

                        if (DebugBoid)
                        {
                            DebugFriends[other.Uid] = new DebugFriend(other, otherLoc, 2);
                        }
                    }

                    if (dist < Owner.Constants.TurnTowardsZone)
                    {
                        turnTowardsAverage += otherLoc;
                        turnTowardsCount++;

                        if (DebugBoid)
                        {
                            DebugFriends[other.Uid] = new DebugFriend(other, otherLoc, 1);
                        }
                    }
                }
            }

            if (turnAwayFromCount != 0)
            {
                turnAwayFromAverage /= turnAwayFromCount;
                TurnAwayFrom(turnAwayFromAverage, Owner.Constants.TurnAwayFromAngle);

                if (Owner.Constants.OrientationMethod == OrientationMethod.Prioritise)
                {
                    alignWithCount = 0;
                    turnTowardsCount = 0;
                }
            }

            if (alignWithCount != 0)
            {
                alignWithAverage /= alignWithCount;

                if (alignWithAverage.X != 0 && alignWithAverage.Y != 0)
                {
                    TurnTowards(alignWithAverage.GetAngle(), Owner.Constants.AlignWithAngle);
                }

                if (Owner.Constants.OrientationMethod == OrientationMethod.Prioritise)
                {
                    turnTowardsCount = 0;
                }
            }

            if (turnTowardsCount != 0)
            {
                turnTowardsAverage /= turnTowardsCount;
                TurnTowards(turnTowardsAverage, Owner.Constants.TurnTowardsAngle);
            }

            if (Informed != InformedStatus.Naive)
            {
                switch (Owner.Constants.InformedMethod)
                {
                    case InformedMethod.Direction:
                        if (Owner.Constants.SplitInformation)
                        {
                            double a = (Informed == InformedStatus.Alpha) ? 0 : Math.PI;
                            TurnTowards(a, Owner.Constants.InformedAngle);
                        }
                        else
                        {
                            TurnTowards(0, Owner.Constants.InformedAngle);
                        }
                        break;

                    case InformedMethod.FloatingPoint:
                    case InformedMethod.Point:
                        if (Owner.Constants.SplitInformation)
                        {
                            double x = (Informed == InformedStatus.Alpha) ? Owner.Predators[0].Location.X : Location.X;
                            double y = (Informed == InformedStatus.Beta) ? Owner.Predators[0].Location.Y : Location.Y;
                            TurnTowards(new Vector(x, y), Owner.Constants.InformedAngle);
                        }
                        else
                        {
                            TurnTowards(Owner.Predators[0].Location, Owner.Constants.InformedAngle);
                        }
                        break;
                }
            }

            if (Rnd.Chance(1d / Owner.Constants.RandomiseInterval))
            {
                switch (Owner.Constants.RandomMethod)
                {
                    case RandomMethod.Recalculate:
                        Angle += (Rnd.Random.NextDouble() * Owner.Constants.RandomiseAngle * 2) - Owner.Constants.RandomiseAngle;
                        break;

                    case RandomMethod.Fixed:
                        TurnTowards(RandomAngle, Owner.Constants.RandomiseAngle);
                        break;

                    case RandomMethod.Global:
                        TurnTowards(Math.PI, Owner.Constants.RandomiseAngle);
                        break;
                }
            }


            if (!Owner.Constants.WrapScreen)
            {
                if (Location.X < 16)
                {
                    if (Location.X < 0)
                    {
                        Angle = 0;
                    }
                    else
                    {
                        TurnTowards(0, Math.PI / 4);
                    }
                }

                if (Location.X > Owner.maxX - 16)
                {
                    if (Location.X > Owner.maxX)
                    {
                        Angle = Math.PI;
                    }
                    else
                    {
                        TurnTowards(Math.PI, Math.PI / 4);
                    }
                }

                if (Location.Y < 16)
                {
                    if (Location.Y < 0)
                    {
                        Angle = Math.PI / 2;
                    }
                    else
                    {
                        TurnTowards(Math.PI / 2, Math.PI / 4);
                    }
                }

                if (Location.Y > Owner.maxY - 16)
                {
                    if (Location.Y > Owner.maxY)
                    {
                        Angle = -Math.PI / 2;
                    }
                    else
                    {
                        TurnTowards(-Math.PI / 2, Math.PI / 4);
                    }
                }
            }

            Walk();
        }

        public Vector GetPercievedLocation(Boid other)
        {
            Vector otherLoc = other.Location;

            if (Owner.Constants.WrapScreen)
            {
                if (Location.X > Owner.xb2 && otherLoc.X < Owner.xb1)
                {
                    otherLoc.X += Owner.maxX;
                }
                else if (Location.X < Owner.xb1 && otherLoc.X > Owner.xb2)
                {
                    otherLoc.X -= Owner.maxX;
                }

                if (Location.Y > Owner.yb2 && otherLoc.Y < Owner.yb1)
                {
                    otherLoc.Y += Owner.maxY;
                }
                else if (Location.Y < Owner.yb1 && otherLoc.Y > Owner.yb2)
                {
                    otherLoc.Y -= Owner.maxY;
                }
            }

            return otherLoc;
        }

        private void Walk()
        {
            Location.X += Math.Cos(Angle) * Owner.Constants.Speed;
            Location.Y += Math.Sin(Angle) * Owner.Constants.Speed;

            if (Owner.Constants.WrapScreen == true)
            {
                if (Location.X < 0)
                {
                    Location.X += Owner.maxX;
                }

                if (Location.Y < 0)
                {
                    Location.Y += Owner.maxY;
                }

                if (Location.X >= Owner.maxX)
                {
                    Location.X -= Owner.maxX;
                }

                if (Location.Y >= Owner.maxY)
                {
                    Location.Y -= Owner.maxY;
                }
            }
        }

        public void Reset()
        {
            Angle = (Rnd.Random.NextDouble() * Math.PI * 2) - Math.PI;
            this.Location.X = Rnd.Random.NextDouble() * Owner.maxX;
            this.Location.Y = Rnd.Random.NextDouble() * Owner.maxY;
            //this.Location.X = 0.5 * Owner.maxX + Rnd.Random.NextDouble() * 64;
            //this.Location.Y = 0.5 * Owner.maxY + Rnd.Random.NextDouble() * 64;
        }
    }
}
