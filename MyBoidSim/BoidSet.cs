﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MyBoidSim
{
    class BoidSet
    {
        public object BoidLock = new object();
        public double maxX;
        public double maxY;
        public List<Boid> Boids;
        public double xb1;
        public double yb1;
        public double xb2;
        public double yb2;
        public Constants Constants;
        public int Age;

        public double EntropySpacial;
        public double EntropyAngular;
        public double EntropyProximity;

        public List<Predator> Predators;

        public const int SegmentGroups = 4; // {All, Uninformed, AlphaInformed, BetaInformed}
        public const int SegmentTypes = 3; // {Spacial, Angular, Proximity}
        public int[, ,] SegmentsSpacial; // Count = SegmentGroup, X, Y
        public int[,] SegmentsAngular; // Count = SegmentGroup, Angle
        public int[,] SegmentsTargetProximity; // Count = SegmentGroup, {Near, Far}
        public int[,] BoidSegment; // Segment = Boid, SegmentType

        public BoidSet(int maxX, int maxY)
        {
            this.maxX = maxX;
            this.maxY = maxY;
            xb1 = maxX * 0.25;
            xb2 = maxX * 0.75;
            yb1 = maxY * 0.25;
            yb2 = maxY * 0.75;

            Predators = new List<Predator>();
            Predators.Add(new Predator(this, maxX / 2, maxY / 2));

            Constants = new Constants();
            Boids = new List<Boid>(Constants.NumberOfBoids);
            SegmentsSpacial = new int[SegmentGroups, Constants.SegmentsSpacialX, Constants.SegmentsSpacialY];
            SegmentsAngular = new int[SegmentGroups, Constants.SegmentsAngleCount];
            SegmentsTargetProximity = new int[SegmentGroups, 2];
            BoidSegment = new int[Boids.Count, SegmentTypes];
        }

        internal void Process()
        {
            // Reset (optional)
            if (Constants.Reset)
            {
                OpenSegmentFile();
                Boids.Clear();
                Age = 0;
                Constants.Reset = false;
            }

            // Food source movement
            if (Constants.InformedMethod == InformedMethod.FloatingPoint)
            {
                foreach (Predator d in Predators)
                {
                    d.Process();
                }
            }

            // Create new boids
            if (Boids.Count < Constants.NumberOfBoids)
            {
                if (Constants.BoidChangeMode == BoidChangeMode.Immediate)
                {
                    while (Boids.Count < Constants.NumberOfBoids)
                    {
                        Boids.Add(new Boid(this, Boids.Count));
                    }
                }
                else
                {
                    Boids.Add(new Boid(this, Boids.Count));
                }
            }

            // Remove surplus boids
            if (Boids.Count > Constants.NumberOfBoids)
            {
                if (Constants.BoidChangeMode == BoidChangeMode.Immediate)
                {
                    while (Boids.Count > Constants.NumberOfBoids)
                    {
                        Boids.RemoveAt(Boids.Count - 1);
                    }
                }
                else
                {
                    Boids.RemoveAt(Boids.Count - 1);
                }
            }

            // Process boids
            foreach (Boid boid in Boids)
            {
                boid.Process();
            }

            // Entropy calculations
            CalculateSegments();

            Age++;
        }

        private void CalculateSegments()
        {
            if (Constants.SegmentUpdate == 0 || ((Age % Constants.SegmentUpdate) != 0))
            {
                return;
            }

            // Check lengths
            if (SegmentsSpacial.GetLength(1) != Constants.SegmentsSpacialX || SegmentsSpacial.GetLength(2) != Constants.SegmentsSpacialY)
            {
                SegmentsSpacial = new int[SegmentGroups, Constants.SegmentsSpacialX, Constants.SegmentsSpacialY];
            }

            if (SegmentsAngular.GetLength(1) != Constants.SegmentsAngleCount)
            {
                SegmentsAngular = new int[SegmentGroups, Constants.SegmentsAngleCount];
            }

            if (BoidSegment.GetLength(0) != Boids.Count)
            {
                BoidSegment = new int[Boids.Count, SegmentTypes];
            }

            // Zero data
            for (int x = 0; x < Constants.SegmentsSpacialX; x++)
            {
                for (int y = 0; y < Constants.SegmentsSpacialY; y++)
                {
                    for (int g = 0; g < SegmentGroups; g++)
                    {
                        SegmentsSpacial[g, x, y] = 0;
                    }
                }
            }

            for (int a = 0; a < Constants.SegmentsAngleCount; a++)
            {
                for (int g = 0; g < SegmentGroups; g++)
                {
                    SegmentsAngular[g, a] = 0;
                }
            }

            for (int g = 0; g < SegmentGroups; g++)
            {
                SegmentsTargetProximity[g, 0] = 0;
                SegmentsTargetProximity[g, 1] = 0;
            }

            // Sum data
            foreach (Boid boid in Boids)
            {
                // Spacial
                int segmentX = (int)((boid.Location.X / maxX) * Constants.SegmentsSpacialX);
                int segmentY = (int)((boid.Location.Y / maxY) * Constants.SegmentsSpacialY);
                segmentX = Clamp(segmentX, 0, Constants.SegmentsSpacialX - 1);
                segmentY = Clamp(segmentY, 0, Constants.SegmentsSpacialY - 1);

                // Angular
                double angle = Boid.WrapAngle(boid.Angle) + Math.PI;
                if (angle == Math.PI * 2) angle = 0;
                int segmentA = (int)((angle / (Math.PI * 2)) * Constants.SegmentsAngleCount);

                // Proximity
                bool proximity = Predators[0].Location.DistanceSquared(boid.Location) <= Constants.SegmentsTargetProximity;

                // Group
                int group = ((int)boid.Informed) + 1;

                // Store data
                BoidSegment[boid.index, 0] = segmentX + segmentY * Constants.SegmentsSpacialX;
                BoidSegment[boid.index, 1] = segmentA;
                BoidSegment[boid.index, 2] = proximity ? 1 : 0;
                SegmentsSpacial[0, segmentX, segmentY]++;
                SegmentsSpacial[group, segmentX, segmentY]++;
                SegmentsAngular[0, segmentA]++;
                SegmentsAngular[group, segmentA]++;
                SegmentsTargetProximity[0, proximity ? 1 : 0]++;
                SegmentsTargetProximity[group, proximity ? 1 : 0]++;
            }

            // Calculate entropy (optional)
            if (Constants.MonitorEntropy)
            {
                var s = MyBoidSim.Entropy.CalculateProbabilities(EnumerateSegmentsSpacial(0), Constants.SegmentsSpacialX * Constants.SegmentsSpacialY);
                var a = MyBoidSim.Entropy.CalculateProbabilities(EnumerateSegmentsAngular(0), Constants.SegmentsAngleCount);
                var p = MyBoidSim.Entropy.CalculateProbabilities(EnumerateSegmentsProximity(0), 2);

                EntropySpacial = MyBoidSim.Entropy.CalculateEntropy(s);
                EntropyAngular = MyBoidSim.Entropy.CalculateEntropy(a);
                EntropyProximity = MyBoidSim.Entropy.CalculateEntropy(p);
            }

            // Save data (optional)
            SaveSegmentData();
        }

        SegmentWriter fileBoids;

        public bool IsRecording
        {
            get { return fileBoids != null; }
        }

        string fileIdName = "";
        public string SegmentFilePrefix = "";

        private void OpenSegmentFile()
        {
            CloseFiles();

            if (Constants.RecordSegments)
            {
                DateTime d = DateTime.Now;

                int fileId = 0;
                string dir;

                do
                {
                    fileId++;
                    dir = Path.Combine(Constants.RecordSegmentsDir, SegmentFilePrefix + fileId.ToString());
                }
                while (Directory.Exists(dir));

                fileIdName = SegmentFilePrefix + fileId.ToString();

                System.IO.Directory.CreateDirectory(dir);

                Constants.Save(Path.Combine(dir, "C.xml"));
                string[] ss = new string[] { "All", "Naive", "Alpha", "Beta" };
                fileBoids = new SegmentWriter(Path.Combine(dir, "Boids"), "Boid", new string[] { "Spacial", "Angular", "Proximity" }, Boids.Count); // Spacial, Angular, Proximity

                using (StreamWriter sw = new StreamWriter(Path.Combine(dir, "BoidTypes.csv")))
                {
                    sw.WriteLine("Index,Informed");

                    foreach (Boid b in Boids)
                    {
                        sw.WriteLine(b.index + "," + (int)b.Informed);
                    }
                }

                using (StreamWriter sw = new StreamWriter(Path.Combine(dir, "SegmentTypes.csv")))
                {
                    sw.WriteLine("Name,Segments");

                    sw.WriteLine("0," + Constants.SegmentsSpacialX * Constants.SegmentsSpacialY); // Spatial
                    sw.WriteLine("1," + Constants.SegmentsAngleCount); // Angular
                    sw.WriteLine("2," + 2); // Proximity
                }
            }
        }

        private void SaveSegmentData()
        {
            if (IsRecording)
            {
                for (int b = 0; b < Boids.Count; b++)
                {
                    fileBoids.Write(0, BoidSegment[b, 0]); // Spacial
                    fileBoids.Write(1, BoidSegment[b, 1]); // Angular
                    fileBoids.Write(2, BoidSegment[b, 2]); // Proximity
                }

                fileBoids.WriteLine();
            }
        }

        class SegmentWriter
        {
            StreamWriter[] sw;
            bool[] newLine;

            public SegmentWriter(string fn, string colPrefix, string[] fileNames, int colCount)
            {
                sw = new StreamWriter[fileNames.Length];
                newLine = new bool[fileNames.Length];

                for (int n = 0; n < fileNames.Length; n++)
                {
                    sw[n] = new StreamWriter(fn + fileNames[n] + ".csv");

                    //sw[n].Write("Age");
                    for (int col = 0; col < colCount; col++)
                    {
                        if (col != 0)
                        {
                            sw[n].Write(",");
                        }
                        sw[n].Write(colPrefix + col);
                    }
                    sw[n].WriteLine();
                    newLine[n] = true;
                }
            }

            public void Write(int g, int v)
            {
                if (!newLine[g])
                {
                    sw[g].Write(",");
                }

                newLine[g] = false;


                sw[g].Write(v);
            }

            public void WriteLine()
            {
                for (int g = 0; g < sw.Length; g++)
                {
                    sw[g].WriteLine();
                    newLine[g] = true;
                }
            }

            public void Close()
            {
                for (int g = 0; g < sw.Length; g++)
                {
                    sw[g].Close();
                }
            }
        }

        private IEnumerable<int> EnumerateSegmentsSpacial(int g)
        {
            for (int x = 0; x < Constants.SegmentsSpacialX; x++)
            {
                for (int y = 0; y < Constants.SegmentsSpacialY; y++)
                {
                    yield return SegmentsSpacial[g, x, y];
                }
            }
        }

        private IEnumerable<int> EnumerateSegmentsAngular(int g)
        {
            for (int a = 0; a < Constants.SegmentsAngleCount; a++)
            {
                yield return SegmentsAngular[g, a];
            }
        }

        private IEnumerable<int> EnumerateSegmentsProximity(int g)
        {
            yield return SegmentsTargetProximity[g, 0];
            yield return SegmentsTargetProximity[g, 1];
        }

        private static int Clamp(int v, int min, int max)
        {
            if (v < min)
            {
                return min;
            }
            else if (v > max)
            {
                return max;
            }

            return v;
        }

        internal string GetInfo()
        {
            string prefix = "";

            if (IsRecording)
            {
                prefix = "[RECORDING: " + fileIdName + "]\r\n";
            }

            return prefix + "Age = " + Age + "\r\nBoids = " + Boids.Count + "\r\nEntropy:\r\n    S = " + EntropySpacial + "\r\n    A = " + EntropyAngular + "\r\n    P = " + EntropyProximity;
        }

        internal void CloseFiles()
        {
            if (IsRecording)
            {
                fileBoids.Close();
                fileBoids = null;
            }
        }
    }
}
