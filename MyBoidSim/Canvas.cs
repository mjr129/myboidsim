﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    class Canvas
    {
        public Bitmap _buffer;
        Bitmap _background;
        VectorInt _offset;
        double _zoom;
        BoidSet _boids;
        Font _font;
        public Boid _infoBoid;
        float _spin;

        public Canvas(BoidSet boids, int margin, double scale)
        {
            this._font = new Font("Arial", 12, FontStyle.Regular);
            this._boids = boids;
            this._offset = new VectorInt(margin, margin);
            this._zoom = scale;
            this._buffer = new Bitmap(_offset.X * 2 + ToScreenSize(boids.maxX), _offset.Y * 2 + ToScreenSize(boids.maxY));
            this._background = new Bitmap(_buffer.Width, _buffer.Height);

            ClearTrails();
        }

        public void ClearTrails()
        {
            using (Graphics g = Graphics.FromImage(_background))
            {
                g.Clear(Color.FromArgb(0, 0, 0));
            }
        }

        public VectorInt ToBoidSpace(VectorInt screenSpace)
        {
            return new VectorInt(
               (int)((screenSpace.X - _offset.X) / _zoom),
               (int)((screenSpace.Y - _offset.Y) / _zoom));
        }

        public int ToScreenSize(int boidSize)
        {
            return (int)(boidSize * _zoom);
        }

        public int ToScreenSize(double boidSize)
        {
            return (int)(boidSize * _zoom);
        }

        public VectorInt ToScreenSpace(Vector boidSpace)
        {
            return new VectorInt(
                (int)(boidSpace.X * _zoom + _offset.X),
                (int)(boidSpace.Y * _zoom + _offset.Y));
        }

        public VectorInt ToScreenSpace(VectorInt boidSpace)
        {
            return new VectorInt(
                (int)(boidSpace.X * _zoom + _offset.X),
                (int)(boidSpace.Y * _zoom + _offset.Y));
        }

        public void DrawBuffer()
        {
            _spin += 0.5f;

            try
            {
                StringFormat rtl = new StringFormat(StringFormatFlags.DirectionRightToLeft);

                using (Graphics g = Graphics.FromImage(_buffer))
                {
                    if (_boids.Constants.DrawTrails)
                    {
                        g.DrawImageUnscaled(_background, 0, 0);
                    }
                    else
                    {
                        g.Clear(Color.Black);
                    }

                    g.DrawRectangle(Pens.Gray, _offset.X, _offset.Y, ToScreenSize(_boids.maxX), ToScreenSize(_boids.maxY));

                    g.DrawString(_boids.GetInfo(), _font, Brushes.White, 0, 0);

                    DrawSegments(g);

                    if (_infoBoid != null)
                    {
                        g.DrawString(_infoBoid.GetInfo(), _font, Brushes.White, _buffer.Width, 0, rtl);
                    }

                    int t = ((_boids.Age / 16) % 360);
                    Color c = ColorWheel.HsvToRgb(t, 1, 0.5);

                    if (_boids.Constants.InformedMethod == InformedMethod.FloatingPoint || _boids.Constants.InformedMethod == InformedMethod.Point)
                    {
                        foreach (Predator p in _boids.Predators)
                        {
                            var v = ToScreenSpace(p.Location);
                            g.DrawEllipse(Pens.Red, v.X - 4, v.Y - 4, 8, 8);
                        }
                    }

                    for (int n = 0; n < _boids.Boids.Count; n++)
                    {
                        Boid boid = _boids.Boids[n];
                        Pen pen;

                        switch (boid.Informed)
                        {
                            case InformedStatus.Naive: pen = penBoid; break;
                            case InformedStatus.Alpha: pen = penBoidInformedX; break;
                            case InformedStatus.Beta: pen = penBoidInformedY; break;
                            default: throw new InvalidOperationException();
                        }

                        VectorInt location = ToScreenSpace(boid.Location);

                        if (_boids.Constants.TrailMode == TrailMode.All)
                        {
                            _background.SetPixel(location.X, location.Y, c);
                        }

                        g.DrawEllipse(pen, location.X - 1, location.Y - 1, 3, 3);

                        float fx = (float)(Math.Cos(boid.Angle) * 5);
                        float fy = (float)(Math.Sin(boid.Angle) * 5);

                        g.DrawLine(pen, location.X, location.Y, location.X + fx, location.Y + fy);

                        if (boid == _infoBoid)
                        {
                            if (_boids.Constants.TrailMode == TrailMode.Selected)
                            {
                                _background.SetPixel(location.X, location.Y, c);
                            }

                            DrawCrosshair(g, location, 10, dpen);

                            DrawZone(g, location, Pens.Gray, _boids.Constants.TurnTowardsZone);
                            DrawZone(g, location, Pens.Gray, _boids.Constants.AlignWithZone);
                            DrawZone(g, location, Pens.Gray, _boids.Constants.TurnAwayFromZone);

                            if (_boids.Constants.ViewAngle != Math.PI)
                            {
                                DrawLine(g, Pens.Gray, location, boid.Angle - _boids.Constants.ViewAngle, (int)Math.Sqrt(_boids.Constants.TurnTowardsZone));
                                DrawLine(g, Pens.Gray, location, boid.Angle + _boids.Constants.ViewAngle, (int)Math.Sqrt(_boids.Constants.TurnTowardsZone));
                            }

                            foreach (DebugFriend d in boid.DebugFriends.Values)
                            {
                                VectorInt v = ToScreenSpace(d.Boid.Location);

                                Pen p = d.Zone == 1 ? dpenz1
                                    : d.Zone == 2 ? dpenz2
                                    : dpenz3;

                                g.DrawRectangle(p, v.X - 4, v.Y - 4, 8, 8);

                                //DrawCrosshair(g, v, 2, dpen);

                                v = ToScreenSpace(d.PercievedLocation);
                                DrawCrosshair(g, v, 2, d2pen);
                            }
                        }
                    } // for each boid
                }
            }
            catch (Exception)
            {
                // TODO: Fix "in use elsewhere" error
            }
        }

        private void DrawSegments(Graphics g)
        {
            if (!_boids.Constants.DrawSegments)
            {
                return;
            }

            if (_boids.Constants.SegmentUpdate != 0)
            {
                // Spacial
                double segLengthX = ToScreenSize(_boids.maxX) / _boids.Constants.SegmentsSpacialX;
                double segLengthY = ToScreenSize(_boids.maxY) / _boids.Constants.SegmentsSpacialY;

                for (int x = 0; x < _boids.Constants.SegmentsSpacialX; x++)
                {
                    for (int y = 0; y < _boids.Constants.SegmentsSpacialY; y++)
                    {
                        int x1 = (int)(x * segLengthX);
                        int y1 = (int)(y * segLengthY);

                        int full = (_boids.SegmentsSpacial[0, x, y] * 255) / _boids.Boids.Count;
                        int fullAmp = Math.Min((full * (_boids.Constants.SegmentsSpacialX * _boids.Constants.SegmentsSpacialY)) / 25, 255);

                        if (_boids.Constants.SegmentsSpacialY == 1)
                        {
                            // Draw bottom bar
                            using (var b = new SolidBrush(Color.FromArgb(full, full, full)))
                            using (var b2 = new SolidBrush(Color.FromArgb(fullAmp, fullAmp, fullAmp)))
                            {
                                g.FillRectangle(b, _offset.X + x1, _offset.Y + ToScreenSize(_boids.maxY) + 1, (int)segLengthX, 16);
                                g.FillRectangle(b2, _offset.X + x1 + 4, _offset.Y + ToScreenSize(_boids.maxY) + 1 + 4, (int)segLengthX - 8, 8);
                            }
                        }
                        else
                        {
                            using (var b = new SolidBrush(Color.FromArgb(fullAmp, 255, 255, 255)))
                            {
                                g.FillRectangle(b, _offset.X + x1, _offset.Y + y1, (int)segLengthX, (int)segLengthY);
                            }
                        }
                    }
                }

                // Angular
                double segLengthA = 360 / _boids.Constants.SegmentsAngleCount;
                Rectangle fRect = new Rectangle(0, _buffer.Height - 128, 128, 128);
                VectorInt o = new VectorInt(64, _buffer.Height - 64);

                g.DrawRectangle(Pens.White, fRect);
                fRect.Inflate(-8, -8);

                for (int x = 0; x < _boids.Constants.SegmentsAngleCount; x++)
                {
                    double a = ((x + 0.5d) * Math.PI * 2d / _boids.Constants.SegmentsAngleCount) - Math.PI;

                    int full = (_boids.SegmentsAngular[0, x] * 255) / _boids.Boids.Count;
                    int fullAmp = Math.Min((int)((full * (_boids.Constants.SegmentsAngleCount)) / 2.5), 255);

                    using (Pen pen = new Pen(Color.FromArgb(fullAmp, fullAmp, fullAmp)))
                    {
                        pen.Width = 3;
                        DrawLine(g, pen, o, a, 64);
                        DrawLine(g, Pens.Gray, o, a, 32);
                    }
                }

                // Proximity
                int full0 = (_boids.SegmentsAngular[0, 0] * 255) / _boids.Boids.Count;
                int full1 = (_boids.SegmentsAngular[0, 1] * 255) / _boids.Boids.Count;
                int fullAmp0 = Math.Min((int)((full0 * (_boids.Constants.SegmentsAngleCount)) / 2.5), 255);
                int fullAmp1 = Math.Min((int)((full1 * (_boids.Constants.SegmentsAngleCount)) / 2.5), 255);

                using (var b = new SolidBrush(Color.FromArgb(fullAmp0, 255, 255, 255)))
                {
                    g.FillRectangle(b, _buffer.Width - 32, _buffer.Height-32, (int)32, (int)32);
                }

                using (var b = new SolidBrush(Color.FromArgb(fullAmp1, 255, 255, 255)))
                {
                    g.FillRectangle(b, _buffer.Width - 32, _buffer.Height - 64, (int)32, (int)32);
                }
            }
        }

        private void DrawLine(Graphics g, Pen pen, VectorInt origin, double angle, int length)
        {
            int dx = (int)(Math.Cos(angle) * length);
            int dy = (int)(Math.Sin(angle) * length);
            g.DrawLine(pen, origin.X, origin.Y, origin.X + dx, origin.Y + dy);
        }

        private static VectorInt DrawZone(Graphics g, VectorInt location, Pen pr, double z)
        {
            z = Math.Sqrt(z);
            g.DrawEllipse(pr, (int)(location.X - z), (int)(location.Y - z), (int)(z * 2), (int)(z * 2));
            return location;
        }

        Pen epen = new Pen(Color.FromArgb(64, 255, 255, 255));
        Pen penBoid = new Pen(Color.FromArgb(255, 0, 255, 0));
        Pen penBoidInformedX = new Pen(Color.FromArgb(255, 255, 0, 255));
        Pen penBoidInformedY = new Pen(Color.FromArgb(255, 0, 255, 255));
        Pen dpen = new Pen(Color.FromArgb(255, 255, 255, 255));
        Pen dpenz1 = new Pen(Color.FromArgb(255, 255, 255, 0));
        Pen dpenz2 = new Pen(Color.FromArgb(255, 0, 255, 255));
        Pen dpenz3 = new Pen(Color.FromArgb(255, 255, 0, 255));
        Pen d2pen = new Pen(Color.FromArgb(255, 255, 0, 0));

        private VectorInt DrawCrosshair(Graphics g, VectorInt v, int size, Pen p)
        {
            int s2 = size * 2;
            //g.DrawLine(Pens.White, v.X - s2, v.Y, v.X - size, v.Y);
            //g.DrawLine(Pens.White, v.X + s2, v.Y, v.X + size, v.Y);
            //g.DrawLine(Pens.White, v.X, v.Y - s2, v.X, v.Y - size);
            //g.DrawLine(Pens.White, v.X, v.Y + s2, v.X, v.Y + size);

            DrawLineOut(g, p, v, size, size * 2, _spin);
            DrawLineOut(g, p, v, size, size * 2, (float)Math.PI / 2 + _spin);
            DrawLineOut(g, p, v, size, size * 2, (float)Math.PI + _spin);
            DrawLineOut(g, p, v, size, size * 2, (float)-Math.PI / 2 + _spin);

            return v;
        }

        static void DrawLineOut(Graphics g, Pen p, VectorInt v, float start, float end, float angle)
        {
            float c = (float)Math.Cos(angle);
            float s = (float)Math.Sin(angle);
            g.DrawLine(p, v.X + c * start, v.Y + s * start, v.X + c * end, v.Y + s * end);
        }
    }
}
