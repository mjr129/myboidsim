﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    public class Constants
    {
        public double TurnTowardsAngle = Degree * 4;
        public double AlignWithAngle = Degree * 2;
        public double TurnAwayFromAngle = Degree * 1;
        public double TurnTowardsZone = 128 * 128;
        public double AlignWithZone = 64 * 64;
        public double TurnAwayFromZone = 16 * 16;
        public double RandomiseAngle = Degree * 5;
        public int RandomiseInterval = 1;
        public int NumberOfBoids = 250;
        public double ViewAngle = Math.PI;
        public BoidChangeMode BoidChangeMode = MyBoidSim.BoidChangeMode.Immediate;
        public double Speed = 5;
        public bool Reset = false;
        public TrailMode TrailMode = TrailMode.Selected;
        const double Degree = Math.PI / 180;
        public int SegmentsSpacialX = 10;
        public int SegmentsSpacialY = 1;
        public int SegmentsAngleCount = 8;
        public double SegmentsTargetProximity = 128;
        public bool WrapScreen = true;
        public int DrawUpdate = 1;
        public int SegmentUpdate = 0;
        public bool DrawTrails = false;
        public bool DrawSegments = false;
        public OrientationMethod OrientationMethod = OrientationMethod.Prioritise;
        public InformedMethod InformedMethod = InformedMethod.None;
        public bool SplitInformation = false;
        public double InformedAngle = Degree * 2;
        public int InformedRate = 10; // 1 in 10
        public bool MonitorEntropy = false;
        public RandomMethod RandomMethod = RandomMethod.Recalculate;
        public bool RecordSegments = false;
        public string RecordSegmentsDir = System.IO.Path.Combine(System.Windows.Forms.Application.StartupPath, "Recordings");

        public void Save(string fileName)
        {
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(Constants));

            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName))
            {
                xs.Serialize(sw, this);
            }
        }

        public static Constants Load(string fileName)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(Constants));

                using (System.IO.StreamReader sr = new System.IO.StreamReader(fileName))
                {
                    return (Constants)xs.Deserialize(sr);
                }
            }
            catch
            {
                return new Constants();
            }
        }

        internal void Assign(Constants c)
        {
            var t = typeof(Constants);
            foreach (var f in t.GetFields())
            {
                var val = f.GetValue(c);
                f.SetValue(this, val);
            }
        }
    }

    public enum RandomMethod
    {
        Recalculate,
        Fixed,
        Global
    }

    public enum InformedMethod
    {
        None,
        Point,
        FloatingPoint,
        Direction,
    }

    public enum OrientationMethod
    {
        Prioritise,
        Accumulate,
    }

    public enum BoidChangeMode
    {
        Immediate,
        Incremental,
    }

    public enum TrailMode
    {
        None,
        All,
        Selected,
    }
}
