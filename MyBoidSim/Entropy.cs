﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    static class Entropy
    {
        public static IEnumerable<double> CalculateProbabilities(System.Collections.IEnumerable counts, int total)
        {
            double t = (double)total;

            foreach (int i in counts)
            {
                yield return ((double)i) / t;
            }
        }

        public static double CalculateEntropy(IEnumerable<double> probabilities)
        {
            // H(X) = - SUM( p(x) log2 p(x) )

            double s = 0;

            foreach (double p in probabilities)
            {
                double v;

                if (p == 0)
                {
                    // If p == 0 then p * log(p) is also 0
                    // this avoids DBZ error with log(0)
                    v = 0;
                }
                else
                {
                    v = p * Math.Log(p, 2);
                }

                s += v;
            }

            s = -s;

            return s;
        }

        public class MiInfo
        {
            double XEntropy;
            double YEntropy;
            double MutualInformation;

            public MiInfo(double xe, double ye, double mi)
            {
                XEntropy = xe;
                YEntropy = ye;
                MutualInformation = mi;
            }

            public override string ToString()
            {
                return XEntropy.ToString() + "," + YEntropy.ToString() + "," + MutualInformation.ToString();
            }
        }

        /// <summary>
        /// Calculates mutual information for vector X against vector Y.
        /// Each element of each vector is the number of boids matching that criteria.
        /// </summary>
        public static MiInfo CalculateMutualInformation(double[,] xyprobs)
        {
            double[] xprobs = GetMargin(true, xyprobs);
            double[] yprobs = GetMargin(false, xyprobs);
            double sum = 0;

            for (int x = 0; x < xprobs.Length; x++)
            {
                for (int y = 0; y < yprobs.Length; y++)
                {
                    // p(x,y) * log2(p(x)p(y) / p(x,y))

                    double px = xprobs[x];
                    double py = yprobs[y];
                    double pxy = xyprobs[x, y];

                    if (pxy != 0) // if pxy is zero then everything is zero and we don't get a DBZ error
                    {
                        sum += pxy * Math.Log((px * py) / pxy, 2);
                    }
                }
            }

            double mi = -sum;

            return new MiInfo(CalculateEntropy(xprobs), CalculateEntropy(yprobs), mi);
        }

        private static double[] GetMargin(bool cx, double[,] xyprobs)
        {
            double[] r = new double[xyprobs.GetLength(cx ? 0 : 1)];

            for (int x = 0; x < xyprobs.GetLength(0); x++)
            {
                for (int y = 0; y < xyprobs.GetLength(1); y++)
                {
                    if (cx)
                    {
                        r[x] += xyprobs[x, y];
                    }
                    else
                    {
                        r[y] += xyprobs[x, y];
                    }
                }
            }

            return r;
        }
    }
}
