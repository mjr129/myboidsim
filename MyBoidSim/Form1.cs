﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBoidSim
{
    public partial class Form1 : Form
    {
        FrmSettings frmSettings;
        FrmRecord frmRecord;
        Thread proc;
        BoidSet boids;

        volatile bool stop;
        volatile bool stopped;
        int[] exemplars = null;
        ManualResetEvent paused = new ManualResetEvent(true);

        string ssf;

        Canvas canvas;

        public Form1()
        {
            InitializeComponent();

            bool smallScreen = (Screen.PrimaryScreen.Bounds.Width < 1280 || Screen.PrimaryScreen.Bounds.Height < 1024);
            int margin = smallScreen ? 64 : 128;
            double scale = smallScreen ? 0.8d : 1.0d;
                                          
            boids = new BoidSet(1024, 768);
            canvas = new Canvas(boids, margin, scale);
            pictureBox1.Size = canvas._buffer.Size;

            ssf=System.IO.Path.Combine(Application.StartupPath, "boidsettings.xml");
            boids.Constants = Constants.Load(ssf);

            if (smallScreen)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(0, 0);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }
            else
            {
                xToolStripMenuItem.Visible = false;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!stopped)
            {
                stop = true;
                e.Cancel = true;
                ResumeSim();
                return;
            }

            lock (boids.BoidLock)
            {
                boids.CloseFiles();
                boids.Constants.Save(ssf);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (proc == null)
            {
                proc = new Thread(Looper);
                proc.Start();
            }
        }

        object pauseLock = new object();

        private void Looper()
        {
            while (!stop)
            {
                lock (boids.BoidLock)
                {
                    boids.Process();

                    if (frmRecord != null && !frmRecord.IsDisposed)
                    {
                        frmRecord.RecordEntropy(boids.EntropySpacial, boids.EntropyAngular, boids.EntropyProximity);
                    }

                    if ((boids.Constants.DrawUpdate != 0) && ((boids.Age % boids.Constants.DrawUpdate) == 0))
                    {
                        canvas.DrawBuffer();
                    }
                }

                pictureBox1.Invoke((MethodInvoker)delegate { pictureBox1.Refresh(); });

                paused.WaitOne();
            }

            stopped = true;
            this.BeginInvoke((MethodInvoker)delegate { this.Close(); });
        }

        bool PauseSim()
        {
            bool wasNotPaused = paused.WaitOne(0);
            paused.Reset();
            return wasNotPaused;
        }

        void ResumeSim(bool conditional)
        {
            if (conditional)
            {
                ResumeSim();
            }
        }

        void ResumeSim()
        {
            paused.Set();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(canvas._buffer, 0, 0);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            lock (boids.BoidLock)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    Boid closest = null;
                    double dist2 = double.MaxValue;
                    VectorInt mouse = canvas.ToBoidSpace(new VectorInt(e.X, e.Y));

                    if (canvas._infoBoid != null)
                    {
                        canvas._infoBoid.DebugBoid = false;
                    }

                    foreach (Boid boid in boids.Boids)
                    {
                        double d2 = boid.Location.DistanceSquared(mouse);

                        if (d2 < dist2)
                        {
                            dist2 = d2;
                            closest = boid;
                        }
                    }


                    canvas._infoBoid = closest;
                    canvas._infoBoid.DebugBoid = true;
                }
                else if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    if (canvas._infoBoid != null)
                    {
                        canvas._infoBoid.DebugBoid = false;
                    }

                    canvas._infoBoid = null;
                }
            }
        }

        private void clusterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PauseSim();

            lock (boids.BoidLock)
            {
                exemplars = AffinityPropogation.Cluster(boids.Boids, ClusterInfo);

                canvas.DrawBuffer();
            }

            pictureBox1.Refresh();
        }

        double[,] ap_a;
        double[,] ap_r;

        private void ClusterInfo(double[,] a, double[,] r)
        {
            ap_a = a;
            ap_r = r;
            canvas.DrawBuffer();
            pictureBox1.Refresh();
            ap_a = null;
            ap_r = null;
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PauseSim();
        }

        private void resumeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResumeSim();
        }

        private void redrawScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lock (boids.BoidLock)
            {
                canvas.DrawBuffer();
            }

            pictureBox1.Refresh();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            boids.Constants.Reset = true;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmSettings != null && frmSettings.IsDisposed)
            {
                frmSettings = null;
            }

            if (frmSettings == null)
            {
                frmSettings = new FrmSettings(boids.Constants, boids.BoidLock, boids);
            }

            if (frmSettings.Visible)
            {
                frmSettings.Activate();
            }
            else
            {
                frmSettings.Show(this);
            }
        }

        private void clearTrailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            canvas.ClearTrails();
        }

        private void debugAnglesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PauseSim();

            using (var frm = new FrmCheckLogic())
            {
                frm.ShowDialog(this);
            }

            ResumeSim();
        }

        private void optionsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (frmRecord != null && frmRecord.IsDisposed)
            {
                frmRecord = null;
            }

            if (frmRecord == null)
            {
                frmRecord = new FrmRecord(boids.Constants, boids.BoidLock);
            }

            frmRecord.Show(this);
        }

        private void screenshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lock (boids.BoidLock)
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    bool wasRunning = PauseSim();

                    sfd.Filter = "PNG|*.png|BMP|*.bmp|JPG|*.jpg|EMF|*.emf|EXIF|*.exif|GIF|*.gif|ICO|*.ico|TIFF|*.tif|WMF|*.wmf";

                    if (sfd.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        canvas._buffer.Save(sfd.FileName);
                    }

                    ResumeSim(wasRunning);
                }
            }
        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mutualInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool paused = PauseSim();

            using (var frm = new FrmMutualInformation())
            {
                frm.ShowDialog(this);
            }

            ResumeSim(paused);
        }
    }
}
