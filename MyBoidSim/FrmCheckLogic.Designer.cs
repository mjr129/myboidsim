﻿namespace MyBoidSim
{
    partial class FrmCheckLogic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._txtInitial = new System.Windows.Forms.TextBox();
            this._txtTarget = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._txtDelta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._txtResult = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this._txtDiff = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Initial";
            // 
            // _txtInitial
            // 
            this._txtInitial.Location = new System.Drawing.Point(8, 24);
            this._txtInitial.Name = "_txtInitial";
            this._txtInitial.Size = new System.Drawing.Size(100, 20);
            this._txtInitial.TabIndex = 1;
            // 
            // _txtTarget
            // 
            this._txtTarget.Location = new System.Drawing.Point(8, 64);
            this._txtTarget.Name = "_txtTarget";
            this._txtTarget.Size = new System.Drawing.Size(100, 20);
            this._txtTarget.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Target";
            // 
            // _txtDelta
            // 
            this._txtDelta.Location = new System.Drawing.Point(8, 104);
            this._txtDelta.Name = "_txtDelta";
            this._txtDelta.Size = new System.Drawing.Size(100, 20);
            this._txtDelta.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Delta";
            // 
            // _txtResult
            // 
            this._txtResult.Location = new System.Drawing.Point(8, 144);
            this._txtResult.Name = "_txtResult";
            this._txtResult.ReadOnly = true;
            this._txtResult.Size = new System.Drawing.Size(100, 20);
            this._txtResult.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Result";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Calculate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _txtDiff
            // 
            this._txtDiff.Location = new System.Drawing.Point(8, 184);
            this._txtDiff.Name = "_txtDiff";
            this._txtDiff.ReadOnly = true;
            this._txtDiff.Size = new System.Drawing.Size(100, 20);
            this._txtDiff.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Difference";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(176, 192);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Basic checks";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmCheckLogic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 236);
            this.Controls.Add(this.button2);
            this.Controls.Add(this._txtDiff);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._txtResult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._txtDelta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._txtTarget);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._txtInitial);
            this.Controls.Add(this.label1);
            this.Name = "FrmCheckLogic";
            this.Text = "FrmCheckLogic";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _txtInitial;
        private System.Windows.Forms.TextBox _txtTarget;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _txtDelta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _txtResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox _txtDiff;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
    }
}