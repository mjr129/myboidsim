﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBoidSim
{
    public partial class FrmCheckLogic : Form
    {
        public FrmCheckLogic()
        {
            InitializeComponent();
        }

        private void CheckBasic(StringBuilder sb, int p1, int p2, int p3)
        {
            sb.Append("From " + p1 + " to " + p2 + " = ");

            double r1 = ToRadians(p1);
            double r2 = ToRadians(p2);
            double r3 = Boid.Difference(r1, r2);
            int r = ToDegrees(r3);

            sb.Append(r);

            if (r == p3 || (r == -180 && p3 == 180))
            {
                sb.AppendLine();
            }
            else
            {
                sb.AppendLine(" Incorrect!");
            }
        }

        private int ToDegrees(double radians)
        {
            double d = (radians * 180) / Math.PI;

            return (int)d;
        }

        private double ToRadians(int p1)
        {
            return (p1 * Math.PI) / 180;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double initial = ReadRadians(_txtInitial);
            double target = ReadRadians(_txtTarget);
            double delta = ReadRadians(_txtDelta);
            double difference = Boid.Difference(initial, target);
            double result = Boid.TurnAngle(initial, target, delta);

            WriteDegrees(_txtDiff, difference);
            WriteDegrees(_txtResult, result);
        }

        private void WriteDegrees(TextBox t, double radians)
        {
            double d = (radians * 180) / Math.PI;

            t.Text = d.ToString();
        }

        private double ReadRadians(TextBox t)
        {
            double d;

            if (!double.TryParse(t.Text, out d))
            {
                return 0;
            }

            return (d * Math.PI) / 180;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            // 0
            CheckBasic(sb, 0, 0, 0);
            CheckBasic(sb, 0, 360, 0);
            CheckBasic(sb, 0, 180, 180);
            CheckBasic(sb, 0, 90, 90);
            CheckBasic(sb, 0, -270, 90);
            CheckBasic(sb, 0, -90, -90);
            CheckBasic(sb, 0, 270, -90);

            // 90
            CheckBasic(sb, 90, 0, -90);
            CheckBasic(sb, 90, 360, -90);
            CheckBasic(sb, 90, 180, 90);
            CheckBasic(sb, 90, 90, 0);
            CheckBasic(sb, 90, -270, 0);
            CheckBasic(sb, 90, -90, 180);
            CheckBasic(sb, 90, 270, 180);

            // 180
            CheckBasic(sb, 180, 0, 180);
            CheckBasic(sb, 180, 360, 180);
            CheckBasic(sb, 180, 180, 0);
            CheckBasic(sb, 180, 90, -90);
            CheckBasic(sb, 180, -270, -90);
            CheckBasic(sb, 180, -90, 90);
            CheckBasic(sb, 180, 270, 90);

            // 270
            CheckBasic(sb, 270, 0, 90);
            CheckBasic(sb, 270, 360, 90);
            CheckBasic(sb, 270, 180, -90);
            CheckBasic(sb, 270, 90, 180);
            CheckBasic(sb, 270, -270, 180);
            CheckBasic(sb, 270, -90, 0);
            CheckBasic(sb, 270, 270, 0);

            // -90
            CheckBasic(sb, -90, 0, 90);
            CheckBasic(sb, -90, 360, 90);
            CheckBasic(sb, -90, 180, -90);
            CheckBasic(sb, -90, 90, 180);
            CheckBasic(sb, -90, -270, 180);
            CheckBasic(sb, -90, -90, 0);
            CheckBasic(sb, -90, 270, 0);

            MessageBox.Show(this, sb.ToString());
        }
    }
}
