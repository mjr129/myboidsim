﻿namespace MyBoidSim
{
    partial class FrmMutualInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnOpen = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this._btnOpenList = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _btnOpen
            // 
            this._btnOpen.Location = new System.Drawing.Point(8, 8);
            this._btnOpen.Name = "_btnOpen";
            this._btnOpen.Size = new System.Drawing.Size(328, 32);
            this._btnOpen.TabIndex = 0;
            this._btnOpen.Text = "Process single directory...";
            this._btnOpen.UseVisualStyleBackColor = true;
            this._btnOpen.Click += new System.EventHandler(this._btnOpen_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(8, 88);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(840, 446);
            this.listBox1.TabIndex = 3;
            // 
            // _btnOpenList
            // 
            this._btnOpenList.Location = new System.Drawing.Point(8, 48);
            this._btnOpenList.Name = "_btnOpenList";
            this._btnOpenList.Size = new System.Drawing.Size(328, 32);
            this._btnOpenList.TabIndex = 4;
            this._btnOpenList.Text = "Process subdirectories...";
            this._btnOpenList.UseVisualStyleBackColor = true;
            this._btnOpenList.Click += new System.EventHandler(this._btnOpenList_Click);
            // 
            // FrmMutualInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 651);
            this.Controls.Add(this._btnOpenList);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this._btnOpen);
            this.Name = "FrmMutualInformation";
            this.Text = "FrmMutualInformation";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btnOpen;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button _btnOpenList;
    }
}