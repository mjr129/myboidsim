﻿using MyBoidSim.MutualInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBoidSim
{
    public partial class FrmMutualInformation : Form
    {
        public FrmMutualInformation()
        {
            InitializeComponent();
        }

        private void _btnOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Directories|*.*";
                if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                string dir = Path.GetDirectoryName(ofd.FileName);

                ProcessDir(dir);
                listBox1.Items.Add("Done: " + dir);
            }
        }

        private void ProcessDir(string dir)
        {
            MiCalc calc = new MiCalc(dir);

            SegmentType[] sts = new SegmentType[] { SegmentType.Spacial, SegmentType.Angular, SegmentType.Proximity };
            InformedStatus[] its = new InformedStatus[] { InformedStatus.All, InformedStatus.Naive, InformedStatus.Alpha, InformedStatus.Beta };

            using (StreamWriter sw = new StreamWriter(Path.Combine(dir, "OUT-EntropyByAge.csv")))
            {
                foreach (var st in sts)
                {
                    foreach (var it in its)
                    {
                        sw.Write(st.ToString() + "" + it);

                        for (int age = 0; age < calc.MaxAge; age++)
                        {
                            double e = Entropy.CalculateEntropy(
                                MiSegmentMi.CalculateProbabilities(calc,
                                                                   it,
                                                                   st,
                                                                   age,
                                                                   age));

                            sw.Write(",");
                            sw.Write(e);
                        }

                        sw.WriteLine();
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(Path.Combine(dir, "OUT-VariousMi.csv")))
            {
                Entropy.MiInfo mi;

                foreach (var st in sts)
                {
                    mi = Entropy.CalculateMutualInformation(
                        MiSegmentMi.CalculateJointProbabilities(calc,
                                                           InformedStatus.Alpha,
                                                           InformedStatus.Beta,
                                                           st,
                                                           st,
                                                           500,
                                                           int.MaxValue,
                                                           0));
                    sw.WriteLine("Alpha_Beta_" + st + "," + mi);

                    mi = Entropy.CalculateMutualInformation(
                       MiSegmentMi.CalculateJointProbabilities(calc,
                                                          InformedStatus.Alpha,
                                                          InformedStatus.Naive,
                                                          st,
                                                          st,
                                                          500,
                                                          int.MaxValue,
                                                          0));
                    sw.WriteLine("Alpha_Naive_" + st + "," + mi);

                    mi = Entropy.CalculateMutualInformation(
                       MiSegmentMi.CalculateJointProbabilities(calc,
                                                          InformedStatus.Beta,
                                                          InformedStatus.Naive,
                                                          st,
                                                          st,
                                                          500,
                                                          int.MaxValue,
                                                          0));
                    sw.WriteLine("Beta_Naive_" + st + "," + mi);

                    mi = Entropy.CalculateMutualInformation(
                      MiSegmentMi.CalculateJointProbabilities(calc,
                                                         InformedStatus.All,
                                                         InformedStatus.All,
                                                         st,
                                                         st,
                                                         500,
                                                         int.MaxValue,
                                                         0,
                                                         true));
                    sw.WriteLine("Pair_Pair_" + st + "," + mi);
                }

                foreach (var it in its)
                {
                    foreach (var st in sts)
                    {
                        mi = Entropy.CalculateMutualInformation(
                        MiSegmentMi.CalculateJointProbabilities(calc,
                                                           it,
                                                           it,
                                                           SegmentType.Spacial,
                                                           st,
                                                           500,
                                                           int.MaxValue,
                                                           0));
                        sw.WriteLine("Spacial_" + st + "_" + it + "," + mi);

                        mi = Entropy.CalculateMutualInformation(
                            MiSegmentMi.CalculateJointProbabilities(calc,
                                                               it,
                                                               it,
                                                               st,
                                                               st,
                                                               500,
                                                               int.MaxValue,
                                                               -10));
                        sw.WriteLine("Now_10Ago_" + st + "_" + it + "," + mi);

                        mi = Entropy.CalculateMutualInformation(
                          MiSegmentMi.CalculateJointProbabilities(calc,
                                                             it,
                                                             it,
                                                             st,
                                                             st,
                                                             500,
                                                             int.MaxValue,
                                                             -100));
                        sw.WriteLine("Now_100Ago_" + st + "_" + it + "," + mi);
                    }
                }
            }
        }

        private void _btnOpenList_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog ofd = new SaveFileDialog())
            {
                ofd.Filter = "Directories|*.*";
                ofd.FileName = "Select Top Directory";

                if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                string dir = Path.GetDirectoryName(ofd.FileName);

                foreach (string subDir in Directory.GetDirectories(dir))
                {
                    ProcessDir(subDir);
                    listBox1.Items.Add("Done: " + subDir);
                    listBox1.SelectedIndex = listBox1.Items.Count - 1;
                    Application.DoEvents();
                }
            }
        }
    }
}
