﻿namespace MyBoidSim
{
    partial class FrmRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecord));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this._chkSpacial = new System.Windows.Forms.ToolStripButton();
            this._chkAngular = new System.Windows.Forms.ToolStripButton();
            this._chkSmoothed = new System.Windows.Forms.ToolStripButton();
            this._chkProximity = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(713, 464);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._chkSpacial,
            this._chkAngular,
            this._chkProximity,
            this._chkSmoothed});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(713, 25);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // _chkSpacial
            // 
            this._chkSpacial.Checked = true;
            this._chkSpacial.CheckOnClick = true;
            this._chkSpacial.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkSpacial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._chkSpacial.ForeColor = System.Drawing.Color.Lime;
            this._chkSpacial.Image = ((System.Drawing.Image)(resources.GetObject("_chkSpacial.Image")));
            this._chkSpacial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkSpacial.Name = "_chkSpacial";
            this._chkSpacial.Size = new System.Drawing.Size(48, 22);
            this._chkSpacial.Text = "Spacial";
            // 
            // _chkAngular
            // 
            this._chkAngular.CheckOnClick = true;
            this._chkAngular.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._chkAngular.ForeColor = System.Drawing.Color.Red;
            this._chkAngular.Image = ((System.Drawing.Image)(resources.GetObject("_chkAngular.Image")));
            this._chkAngular.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkAngular.Name = "_chkAngular";
            this._chkAngular.Size = new System.Drawing.Size(53, 22);
            this._chkAngular.Text = "Angular";
            // 
            // _chkSmoothed
            // 
            this._chkSmoothed.CheckOnClick = true;
            this._chkSmoothed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._chkSmoothed.ForeColor = System.Drawing.Color.Black;
            this._chkSmoothed.Image = ((System.Drawing.Image)(resources.GetObject("_chkSmoothed.Image")));
            this._chkSmoothed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkSmoothed.Name = "_chkSmoothed";
            this._chkSmoothed.Size = new System.Drawing.Size(66, 22);
            this._chkSmoothed.Text = "Smoothed";
            // 
            // _chkProximity
            // 
            this._chkProximity.CheckOnClick = true;
            this._chkProximity.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._chkProximity.ForeColor = System.Drawing.Color.Blue;
            this._chkProximity.Image = ((System.Drawing.Image)(resources.GetObject("_chkProximity.Image")));
            this._chkProximity.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkProximity.Name = "_chkProximity";
            this._chkProximity.Size = new System.Drawing.Size(61, 22);
            this._chkProximity.Text = "Proximity";
            // 
            // FrmRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(713, 489);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.toolStrip1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmRecord";
            this.Text = "Entropy Graph";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton _chkSpacial;
        private System.Windows.Forms.ToolStripButton _chkAngular;
        private System.Windows.Forms.ToolStripButton _chkSmoothed;
        private System.Windows.Forms.ToolStripButton _chkProximity;
    }
}