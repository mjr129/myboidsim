﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBoidSim
{
    public partial class FrmRecord : Form
    {
        object boidLock;
        Constants constants;
        List<double[]> records = new List<double[]>();
        Bitmap graph;
        int t = 0;

        public FrmRecord(Constants constants, object boidLock)
            : this()
        {
            this.constants = constants;
            this.boidLock = boidLock;
        }

        public FrmRecord()
        {
            InitializeComponent();
        }

        delegate void RecordEntropyS(double a, double b, double c);

        public void RecordEntropy(double a, double b, double c)
        {
            if (InvokeRequired)
            {
                BeginInvoke((RecordEntropyS)RecordEntropy, a, b, c);
                return;
            }

            double[] x = new double[] { a, b, c };

            records.Add(x);

            if (records.Count > 150)
            {
                t++;
                records.RemoveAt(0);
            }

            DrawGraph();

            pictureBox1.Refresh();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (graph != null)
            {
                e.Graphics.DrawImageUnscaled(graph, 0, 0);
            }
        }

        Color colourBackground = Color.FromArgb(0, 64, 0);
        Pen[] penPlot = { new Pen(Color.FromArgb(0, 255, 0)), new Pen(Color.FromArgb(255, 0, 0)), new Pen(Color.FromArgb(0, 0, 255)) };
        Pen penLine = new Pen(Color.FromArgb(0, 128, 0));
        //Pen dm = new Pen(Color.FromArgb(128, 128, 0));

        private void DrawGraph()
        {
            if (graph == null || graph.Width != pictureBox1.ClientSize.Width || graph.Height != pictureBox1.ClientSize.Height)
            {
                if (graph != null)
                {
                    graph.Dispose();
                }

                graph = new Bitmap(pictureBox1.ClientSize.Width, pictureBox1.ClientSize.Height);
            }

            using (Graphics g = Graphics.FromImage(graph))
            {
                g.Clear(colourBackground);

                double min = double.MaxValue;
                double max = double.MinValue;

                foreach (var v in records)
                {
                    if (_chkSpacial.Checked)
                    {
                        min = Math.Min(min, v[0]);
                        max = Math.Max(max, v[0]);
                    }
                    if (_chkAngular.Checked)
                    {
                        min = Math.Min(min, v[1]);
                        max = Math.Max(max, v[1]);
                    }
                    if (_chkProximity.Checked)
                    {
                        min = Math.Min(min, v[2]);
                        max = Math.Max(max, v[2]);
                    }
                }

                if (min == double.MaxValue || min == max)
                {
                    if (_chkSpacial.Checked || _chkAngular.Checked || _chkProximity.Checked)
                    {
                        g.DrawString("No data to display.\r\nCheck entropy is being calculated from the settings menu.", Font, Brushes.White, 0, 0);
                    }
                    else
                    {
                        g.DrawString("No data to display.\r\nSelect at least one dataset to display.", Font, Brushes.White, 0, 0);
                    }

                    return;
                }

                double range = max - min;

                int height = graph.Height - 1;
                int width = graph.Width - 1;
                int zeroY = height - (int)(((0 - min) / range) * height);

                // Draw zero
                g.DrawLine(penLine, 0, zeroY, graph.Width, zeroY);

                for (int n = 1; n < records.Count - 1; n++)
                {
                    var a = GetRecord(n - 1);
                    var b = GetRecord(n);

                    int ax = (int)(((n - 1) / (double)records.Count) * width);
                    int bx = (int)((n / (double)records.Count) * width);

                    if ((t + n) % 50 == 0)
                    {
                        g.DrawLine(penLine, bx, 0, bx, graph.Height);
                    }

                    for (int group = 0; group < 3; group++)
                    {
                        if (!_chkSpacial.Checked && group == 0) continue;
                        if (!_chkAngular.Checked && group == 1) continue;
                        if (!_chkProximity.Checked && group == 2) continue;

                        double av = a[group];
                        double bv = b[group];

                        double ae = av - min;
                        double be = bv - min;

                        int ay = height - (int)((ae / range) * height);
                        int by = height - (int)((be / range) * height);

                        g.DrawLine(penPlot[group], ax, ay, bx, by);
                    }
                }
            }
        }

        private double[] GetRecord(int n)
        {
            if (!_chkSmoothed.Checked)
            {
                return records[n];
            }

            double[] sum = { 0, 0, 0 };
            int c = 0;

            for (int s = 0; s < 10; s++)
            {
                if ((n - s) >= 0)
                {
                    c++;

                    for (int g = 0; g < 3; g++)
                    {
                        sum[g] += records[n - s][g];
                    }
                }
            }

            for (int g = 0; g < 3; g++)
            {
                sum[g] /= c;
            }

            return sum;
        }

        private void _chkReset_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void _chkClear_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
