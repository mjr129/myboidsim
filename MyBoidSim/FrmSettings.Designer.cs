﻿namespace MyBoidSim
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettings));
            this.label1 = new System.Windows.Forms.Label();
            this._sTowDist = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this._sTowAng = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this._sAlnAng = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this._sAlnDist = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._sAvoAng = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this._sAvoDist = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._sFov = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this._sNum = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this._sMov = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._lstChange = new System.Windows.Forms.ComboBox();
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._chkReset = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this._lstTrails = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._chkWrap = new System.Windows.Forms.CheckBox();
            this._sSegX = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this._sSegY = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this._sDrawUpdate = new System.Windows.Forms.NumericUpDown();
            this._sSegUpdate = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this._sDrawTrails = new System.Windows.Forms.CheckBox();
            this._sDrawSegments = new System.Windows.Forms.CheckBox();
            this._sRndAngle = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this._sRndInterval = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this._sOrientationMethod = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this._lstInformed = new System.Windows.Forms.ComboBox();
            this._sInformedAng = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this._sInformedRate = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this._sRandomMethod = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._btnSave = new System.Windows.Forms.Button();
            this._btnLoad = new System.Windows.Forms.Button();
            this._chkInformed = new System.Windows.Forms.CheckBox();
            this._sSegA = new System.Windows.Forms.NumericUpDown();
            this._sSegP = new System.Windows.Forms.NumericUpDown();
            this._btnRecordDir = new System.Windows.Forms.Button();
            this._chkRecord = new System.Windows.Forms.CheckBox();
            this._txtRecordDir = new System.Windows.Forms.TextBox();
            this._chkCalculateEntropy = new System.Windows.Forms.CheckBox();
            this._btnDefaults = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this._btnExploreOutput = new System.Windows.Forms.LinkLabel();
            this._btnRunScript = new System.Windows.Forms.Button();
            this._tmrScript = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._sTowDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sTowAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAlnAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAlnDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAvoAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAvoDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sFov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sMov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sDrawUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sRndAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sRndInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sInformedAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sInformedRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegP)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Turn towards";
            // 
            // _sTowDist
            // 
            this._sTowDist.Location = new System.Drawing.Point(64, 32);
            this._sTowDist.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this._sTowDist.Name = "_sTowDist";
            this._sTowDist.Size = new System.Drawing.Size(120, 20);
            this._sTowDist.TabIndex = 1;
            this.toolTip1.SetToolTip(this._sTowDist, "Distance within which boids aim to orientate towards this target");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Distance";
            // 
            // _sTowAng
            // 
            this._sTowAng.DecimalPlaces = 2;
            this._sTowAng.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sTowAng.Location = new System.Drawing.Point(64, 56);
            this._sTowAng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sTowAng.Name = "_sTowAng";
            this._sTowAng.Size = new System.Drawing.Size(120, 20);
            this._sTowAng.TabIndex = 3;
            this.toolTip1.SetToolTip(this._sTowAng, "Angle by which to orientate towards this target, per frame");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Angle";
            // 
            // _sAlnAng
            // 
            this._sAlnAng.DecimalPlaces = 2;
            this._sAlnAng.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sAlnAng.Location = new System.Drawing.Point(248, 56);
            this._sAlnAng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sAlnAng.Name = "_sAlnAng";
            this._sAlnAng.Size = new System.Drawing.Size(120, 20);
            this._sAlnAng.TabIndex = 8;
            this.toolTip1.SetToolTip(this._sAlnAng, "Angle by which to orientate towards this target, per frame");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(192, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Angle";
            // 
            // _sAlnDist
            // 
            this._sAlnDist.Location = new System.Drawing.Point(248, 32);
            this._sAlnDist.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this._sAlnDist.Name = "_sAlnDist";
            this._sAlnDist.Size = new System.Drawing.Size(120, 20);
            this._sAlnDist.TabIndex = 6;
            this.toolTip1.SetToolTip(this._sAlnDist, "Distance within which boids aim to orientate towards this target");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(192, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Distance";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(192, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Align with";
            // 
            // _sAvoAng
            // 
            this._sAvoAng.DecimalPlaces = 2;
            this._sAvoAng.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sAvoAng.Location = new System.Drawing.Point(432, 56);
            this._sAvoAng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sAvoAng.Name = "_sAvoAng";
            this._sAvoAng.Size = new System.Drawing.Size(120, 20);
            this._sAvoAng.TabIndex = 13;
            this.toolTip1.SetToolTip(this._sAvoAng, "Angle by which to orientate towards this target, per frame");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(376, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Angle";
            // 
            // _sAvoDist
            // 
            this._sAvoDist.Location = new System.Drawing.Point(432, 32);
            this._sAvoDist.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this._sAvoDist.Name = "_sAvoDist";
            this._sAvoDist.Size = new System.Drawing.Size(120, 20);
            this._sAvoDist.TabIndex = 11;
            this.toolTip1.SetToolTip(this._sAvoDist, "Distance within which boids aim to orientate towards this target");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(376, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Distance";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Lime;
            this.label9.Location = new System.Drawing.Point(376, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Turn away from";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Lime;
            this.label10.Location = new System.Drawing.Point(8, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Field of view";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Angle";
            // 
            // _sFov
            // 
            this._sFov.DecimalPlaces = 2;
            this._sFov.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sFov.Location = new System.Drawing.Point(64, 112);
            this._sFov.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sFov.Name = "_sFov";
            this._sFov.Size = new System.Drawing.Size(120, 20);
            this._sFov.TabIndex = 3;
            this.toolTip1.SetToolTip(this._sFov, "Boids\' field of view");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Lime;
            this.label12.Location = new System.Drawing.Point(8, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Boids";
            // 
            // _sNum
            // 
            this._sNum.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this._sNum.Location = new System.Drawing.Point(64, 168);
            this._sNum.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this._sNum.Name = "_sNum";
            this._sNum.Size = new System.Drawing.Size(120, 20);
            this._sNum.TabIndex = 16;
            this.toolTip1.SetToolTip(this._sNum, "Number of boids");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Number";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Lime;
            this.label14.Location = new System.Drawing.Point(192, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Movement";
            // 
            // _sMov
            // 
            this._sMov.DecimalPlaces = 2;
            this._sMov.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sMov.Location = new System.Drawing.Point(248, 112);
            this._sMov.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this._sMov.Name = "_sMov";
            this._sMov.Size = new System.Drawing.Size(120, 20);
            this._sMov.TabIndex = 19;
            this.toolTip1.SetToolTip(this._sMov, "Boids\' speed");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(192, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Velocity";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 192);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Change";
            // 
            // _lstChange
            // 
            this._lstChange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._lstChange.FormattingEnabled = true;
            this._lstChange.Location = new System.Drawing.Point(64, 192);
            this._lstChange.Name = "_lstChange";
            this._lstChange.Size = new System.Drawing.Size(121, 21);
            this._lstChange.TabIndex = 22;
            this.toolTip1.SetToolTip(this._lstChange, "When number of boids is changed how to actuate this change.\r\n* Immediately\r\n* 1 b" +
        "oid added/removed each frame");
            // 
            // _btnOk
            // 
            this._btnOk.ForeColor = System.Drawing.Color.Lime;
            this._btnOk.Location = new System.Drawing.Point(432, 328);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(96, 40);
            this._btnOk.TabIndex = 23;
            this._btnOk.Text = "Apply";
            this.toolTip1.SetToolTip(this._btnOk, "Apply all changes");
            this._btnOk.UseVisualStyleBackColor = false;
            this._btnOk.Click += new System.EventHandler(this._btnOk_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.ForeColor = System.Drawing.Color.Lime;
            this._btnCancel.Location = new System.Drawing.Point(536, 328);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(96, 40);
            this._btnCancel.TabIndex = 24;
            this._btnCancel.Text = "Revert";
            this.toolTip1.SetToolTip(this._btnCancel, "Undo any changes made since last Apply");
            this._btnCancel.UseVisualStyleBackColor = false;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // _chkReset
            // 
            this._chkReset.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._chkReset.Location = new System.Drawing.Point(192, 248);
            this._chkReset.Name = "_chkReset";
            this._chkReset.Size = new System.Drawing.Size(72, 17);
            this._chkReset.TabIndex = 25;
            this._chkReset.Text = "Reset";
            this.toolTip1.SetToolTip(this._chkReset, "Reset simulation when settings are changed");
            this._chkReset.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Lime;
            this.label17.Location = new System.Drawing.Point(192, 224);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "Simulation";
            // 
            // _lstTrails
            // 
            this._lstTrails.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._lstTrails.FormattingEnabled = true;
            this._lstTrails.Location = new System.Drawing.Point(432, 168);
            this._lstTrails.Name = "_lstTrails";
            this._lstTrails.Size = new System.Drawing.Size(121, 21);
            this._lstTrails.TabIndex = 29;
            this.toolTip1.SetToolTip(this._lstTrails, "Which boid trails to draw\r\n* All boids\r\n* Selected boid\r\n* No boids");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(376, 168);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Trails";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Lime;
            this.label20.Location = new System.Drawing.Point(192, 144);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Environment";
            // 
            // _chkWrap
            // 
            this._chkWrap.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._chkWrap.Location = new System.Drawing.Point(192, 168);
            this._chkWrap.Name = "_chkWrap";
            this._chkWrap.Size = new System.Drawing.Size(72, 17);
            this._chkWrap.TabIndex = 31;
            this._chkWrap.Text = "Wrap";
            this.toolTip1.SetToolTip(this._chkWrap, "Whether the environment wraps\r\n* Environment and boid actions wrap pac-man style\r" +
        "\n* Environment does not wrap, boids have slight preference to avoid edges");
            this._chkWrap.UseVisualStyleBackColor = true;
            // 
            // _sSegX
            // 
            this._sSegX.Location = new System.Drawing.Point(616, 168);
            this._sSegX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._sSegX.Name = "_sSegX";
            this._sSegX.Size = new System.Drawing.Size(56, 20);
            this._sSegX.TabIndex = 33;
            this.toolTip1.SetToolTip(this._sSegX, "Number of X segments in which to measure entropy (number of angles if angle metho" +
        "d used)");
            this._sSegX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(560, 168);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "Spacial";
            // 
            // _sSegY
            // 
            this._sSegY.Location = new System.Drawing.Point(680, 168);
            this._sSegY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._sSegY.Name = "_sSegY";
            this._sSegY.Size = new System.Drawing.Size(57, 20);
            this._sSegY.TabIndex = 34;
            this.toolTip1.SetToolTip(this._sSegY, "Number of X segments in which to measure entropy (no effect if angle method used)" +
        "");
            this._sSegY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(376, 240);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "Skip";
            // 
            // _sDrawUpdate
            // 
            this._sDrawUpdate.Location = new System.Drawing.Point(432, 240);
            this._sDrawUpdate.Name = "_sDrawUpdate";
            this._sDrawUpdate.Size = new System.Drawing.Size(120, 20);
            this._sDrawUpdate.TabIndex = 36;
            this.toolTip1.SetToolTip(this._sDrawUpdate, "How often to redraw the screen, in frames");
            // 
            // _sSegUpdate
            // 
            this._sSegUpdate.Location = new System.Drawing.Point(616, 240);
            this._sSegUpdate.Name = "_sSegUpdate";
            this._sSegUpdate.Size = new System.Drawing.Size(56, 20);
            this._sSegUpdate.TabIndex = 39;
            this.toolTip1.SetToolTip(this._sSegUpdate, "How often to calculate entropy, in frames");
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Lime;
            this.label25.Location = new System.Drawing.Point(376, 144);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 13);
            this.label25.TabIndex = 40;
            this.label25.Text = "Drawing";
            // 
            // _sDrawTrails
            // 
            this._sDrawTrails.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._sDrawTrails.Location = new System.Drawing.Point(376, 192);
            this._sDrawTrails.Name = "_sDrawTrails";
            this._sDrawTrails.Size = new System.Drawing.Size(72, 17);
            this._sDrawTrails.TabIndex = 41;
            this._sDrawTrails.Text = "Trails";
            this.toolTip1.SetToolTip(this._sDrawTrails, "View trails on screen");
            this._sDrawTrails.UseVisualStyleBackColor = true;
            // 
            // _sDrawSegments
            // 
            this._sDrawSegments.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._sDrawSegments.Location = new System.Drawing.Point(376, 216);
            this._sDrawSegments.Name = "_sDrawSegments";
            this._sDrawSegments.Size = new System.Drawing.Size(72, 17);
            this._sDrawSegments.TabIndex = 42;
            this._sDrawSegments.Text = "Segments";
            this.toolTip1.SetToolTip(this._sDrawSegments, "View segments used in entropy calculation on screen");
            this._sDrawSegments.UseVisualStyleBackColor = true;
            // 
            // _sRndAngle
            // 
            this._sRndAngle.DecimalPlaces = 2;
            this._sRndAngle.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sRndAngle.Location = new System.Drawing.Point(616, 56);
            this._sRndAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sRndAngle.Name = "_sRndAngle";
            this._sRndAngle.Size = new System.Drawing.Size(120, 20);
            this._sRndAngle.TabIndex = 47;
            this.toolTip1.SetToolTip(this._sRndAngle, "Maximum change in angle upon randomisation");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(560, 56);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 46;
            this.label26.Text = "Angle";
            // 
            // _sRndInterval
            // 
            this._sRndInterval.Location = new System.Drawing.Point(616, 32);
            this._sRndInterval.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this._sRndInterval.Name = "_sRndInterval";
            this._sRndInterval.Size = new System.Drawing.Size(120, 20);
            this._sRndInterval.TabIndex = 45;
            this.toolTip1.SetToolTip(this._sRndInterval, "How often to randomise direction");
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(560, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "Interval";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Lime;
            this.label28.Location = new System.Drawing.Point(560, 8);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "Randomise";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Lime;
            this.label29.Location = new System.Drawing.Point(376, 88);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 13);
            this.label29.TabIndex = 48;
            this.label29.Text = "Orientation";
            // 
            // _sOrientationMethod
            // 
            this._sOrientationMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sOrientationMethod.FormattingEnabled = true;
            this._sOrientationMethod.Location = new System.Drawing.Point(432, 112);
            this._sOrientationMethod.Name = "_sOrientationMethod";
            this._sOrientationMethod.Size = new System.Drawing.Size(121, 21);
            this._sOrientationMethod.TabIndex = 22;
            this.toolTip1.SetToolTip(this._sOrientationMethod, "Rules by which boids determine a direction to take:\r\n* Take only the effect of th" +
        "e highest priority zone (towards < align < avoid).\r\n* Attempt to combine effects" +
        " of all zones the boid is within.");
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Lime;
            this.label30.Location = new System.Drawing.Point(744, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 50;
            this.label30.Text = "Informed";
            // 
            // _lstInformed
            // 
            this._lstInformed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._lstInformed.FormattingEnabled = true;
            this._lstInformed.Location = new System.Drawing.Point(800, 32);
            this._lstInformed.Name = "_lstInformed";
            this._lstInformed.Size = new System.Drawing.Size(121, 21);
            this._lstInformed.TabIndex = 49;
            this.toolTip1.SetToolTip(this._lstInformed, "Where informed boids head\r\n* In a fixed direction\r\n* Towards a fixed target\r\n* To" +
        "wards a moving target\r\n* No preference");
            this._lstInformed.SelectedIndexChanged += new System.EventHandler(this._lstInformed_SelectedIndexChanged);
            // 
            // _sInformedAng
            // 
            this._sInformedAng.DecimalPlaces = 2;
            this._sInformedAng.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this._sInformedAng.Location = new System.Drawing.Point(800, 56);
            this._sInformedAng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this._sInformedAng.Name = "_sInformedAng";
            this._sInformedAng.Size = new System.Drawing.Size(120, 20);
            this._sInformedAng.TabIndex = 52;
            this.toolTip1.SetToolTip(this._sInformedAng, "Angle by which informed boids try to match their target, per frame");
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(744, 56);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 13);
            this.label31.TabIndex = 51;
            this.label31.Text = "Angle";
            // 
            // _sInformedRate
            // 
            this._sInformedRate.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this._sInformedRate.Location = new System.Drawing.Point(800, 80);
            this._sInformedRate.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this._sInformedRate.Name = "_sInformedRate";
            this._sInformedRate.Size = new System.Drawing.Size(120, 20);
            this._sInformedRate.TabIndex = 54;
            this.toolTip1.SetToolTip(this._sInformedRate, "Fraction of boids that are informed");
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(744, 80);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 13);
            this.label32.TabIndex = 53;
            this.label32.Text = "Rate";
            // 
            // _sRandomMethod
            // 
            this._sRandomMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sRandomMethod.FormattingEnabled = true;
            this._sRandomMethod.Location = new System.Drawing.Point(616, 80);
            this._sRandomMethod.Name = "_sRandomMethod";
            this._sRandomMethod.Size = new System.Drawing.Size(121, 21);
            this._sRandomMethod.TabIndex = 57;
            this.toolTip1.SetToolTip(this._sRandomMethod, "Method by which to randomise.\r\n* Random angle each frame\r\n* Random angle that is " +
        "fixed for each boid\r\n* Random angle that is fixed for all boids");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(560, 80);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(43, 13);
            this.label34.TabIndex = 58;
            this.label34.Text = "Method";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(744, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "Target";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(376, 112);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 13);
            this.label35.TabIndex = 12;
            this.label35.Text = "Rules";
            // 
            // _btnSave
            // 
            this._btnSave.ForeColor = System.Drawing.Color.Lime;
            this._btnSave.Location = new System.Drawing.Point(744, 328);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(96, 40);
            this._btnSave.TabIndex = 60;
            this._btnSave.Text = "Save";
            this.toolTip1.SetToolTip(this._btnSave, "Apply all changes");
            this._btnSave.UseVisualStyleBackColor = false;
            this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
            // 
            // _btnLoad
            // 
            this._btnLoad.ForeColor = System.Drawing.Color.Lime;
            this._btnLoad.Location = new System.Drawing.Point(848, 328);
            this._btnLoad.Name = "_btnLoad";
            this._btnLoad.Size = new System.Drawing.Size(96, 40);
            this._btnLoad.TabIndex = 61;
            this._btnLoad.Text = "Load";
            this.toolTip1.SetToolTip(this._btnLoad, "Apply all changes");
            this._btnLoad.UseVisualStyleBackColor = false;
            this._btnLoad.Click += new System.EventHandler(this._btnLoad_Click);
            // 
            // _chkInformed
            // 
            this._chkInformed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._chkInformed.Location = new System.Drawing.Point(744, 104);
            this._chkInformed.Name = "_chkInformed";
            this._chkInformed.Size = new System.Drawing.Size(72, 17);
            this._chkInformed.TabIndex = 62;
            this._chkInformed.Text = "Split information";
            this.toolTip1.SetToolTip(this._chkInformed, resources.GetString("_chkInformed.ToolTip"));
            this._chkInformed.UseVisualStyleBackColor = true;
            // 
            // _sSegA
            // 
            this._sSegA.Location = new System.Drawing.Point(616, 192);
            this._sSegA.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._sSegA.Name = "_sSegA";
            this._sSegA.Size = new System.Drawing.Size(56, 20);
            this._sSegA.TabIndex = 65;
            this.toolTip1.SetToolTip(this._sSegA, "Number of X segments in which to measure entropy (number of angles if angle metho" +
        "d used)");
            this._sSegA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _sSegP
            // 
            this._sSegP.Location = new System.Drawing.Point(616, 216);
            this._sSegP.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this._sSegP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._sSegP.Name = "_sSegP";
            this._sSegP.Size = new System.Drawing.Size(56, 20);
            this._sSegP.TabIndex = 67;
            this.toolTip1.SetToolTip(this._sSegP, "Number of X segments in which to measure entropy (number of angles if angle metho" +
        "d used)");
            this._sSegP.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // _btnRecordDir
            // 
            this._btnRecordDir.BackColor = System.Drawing.Color.White;
            this._btnRecordDir.ForeColor = System.Drawing.Color.Black;
            this._btnRecordDir.Location = new System.Drawing.Point(160, 272);
            this._btnRecordDir.Name = "_btnRecordDir";
            this._btnRecordDir.Size = new System.Drawing.Size(24, 23);
            this._btnRecordDir.TabIndex = 69;
            this._btnRecordDir.Text = "...";
            this.toolTip1.SetToolTip(this._btnRecordDir, "Select where logged data are stored to");
            this._btnRecordDir.UseVisualStyleBackColor = false;
            this._btnRecordDir.Click += new System.EventHandler(this._btnRecordDir_Click);
            // 
            // _chkRecord
            // 
            this._chkRecord.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._chkRecord.Location = new System.Drawing.Point(8, 248);
            this._chkRecord.Name = "_chkRecord";
            this._chkRecord.Size = new System.Drawing.Size(72, 17);
            this._chkRecord.TabIndex = 70;
            this._chkRecord.Text = "Record";
            this.toolTip1.SetToolTip(this._chkRecord, resources.GetString("_chkRecord.ToolTip"));
            this._chkRecord.UseVisualStyleBackColor = true;
            // 
            // _txtRecordDir
            // 
            this._txtRecordDir.Location = new System.Drawing.Point(64, 272);
            this._txtRecordDir.Name = "_txtRecordDir";
            this._txtRecordDir.Size = new System.Drawing.Size(100, 20);
            this._txtRecordDir.TabIndex = 71;
            this.toolTip1.SetToolTip(this._txtRecordDir, "Select where logged data are stored to");
            // 
            // _chkCalculateEntropy
            // 
            this._chkCalculateEntropy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._chkCalculateEntropy.Location = new System.Drawing.Point(560, 264);
            this._chkCalculateEntropy.Name = "_chkCalculateEntropy";
            this._chkCalculateEntropy.Size = new System.Drawing.Size(72, 17);
            this._chkCalculateEntropy.TabIndex = 74;
            this._chkCalculateEntropy.Text = "Entropy";
            this.toolTip1.SetToolTip(this._chkCalculateEntropy, "Calculate entropy on-the-fly and display (Update Segments must be non-zero to be " +
        "effective)");
            this._chkCalculateEntropy.UseVisualStyleBackColor = true;
            // 
            // _btnDefaults
            // 
            this._btnDefaults.ForeColor = System.Drawing.Color.Lime;
            this._btnDefaults.Location = new System.Drawing.Point(640, 328);
            this._btnDefaults.Name = "_btnDefaults";
            this._btnDefaults.Size = new System.Drawing.Size(96, 40);
            this._btnDefaults.TabIndex = 76;
            this._btnDefaults.Text = "Defaults";
            this.toolTip1.SetToolTip(this._btnDefaults, "Undo any changes made since last Apply");
            this._btnDefaults.UseVisualStyleBackColor = false;
            this._btnDefaults.Click += new System.EventHandler(this._btnDefaults_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Lime;
            this.label36.Location = new System.Drawing.Point(560, 144);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(109, 13);
            this.label36.TabIndex = 63;
            this.label36.Text = "Entropy Segments";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(560, 192);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(57, 13);
            this.label37.TabIndex = 64;
            this.label37.Text = "Directional";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(560, 216);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 13);
            this.label38.TabIndex = 66;
            this.label38.Text = "Proximity";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Lime;
            this.label33.Location = new System.Drawing.Point(8, 224);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 13);
            this.label33.TabIndex = 68;
            this.label33.Text = "Recording";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 272);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(39, 13);
            this.label39.TabIndex = 72;
            this.label39.Text = "Output";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(560, 240);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(42, 13);
            this.label40.TabIndex = 75;
            this.label40.Text = "Update";
            // 
            // _btnExploreOutput
            // 
            this._btnExploreOutput.AutoSize = true;
            this._btnExploreOutput.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this._btnExploreOutput.LinkColor = System.Drawing.Color.Aqua;
            this._btnExploreOutput.Location = new System.Drawing.Point(64, 296);
            this._btnExploreOutput.Name = "_btnExploreOutput";
            this._btnExploreOutput.Size = new System.Drawing.Size(42, 13);
            this._btnExploreOutput.TabIndex = 77;
            this._btnExploreOutput.TabStop = true;
            this._btnExploreOutput.Text = "Explore";
            this._btnExploreOutput.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._btnExploreOutput_LinkClicked);
            // 
            // _btnRunScript
            // 
            this._btnRunScript.ForeColor = System.Drawing.Color.Lime;
            this._btnRunScript.Location = new System.Drawing.Point(328, 328);
            this._btnRunScript.Name = "_btnRunScript";
            this._btnRunScript.Size = new System.Drawing.Size(96, 40);
            this._btnRunScript.TabIndex = 78;
            this._btnRunScript.Text = "Run script";
            this.toolTip1.SetToolTip(this._btnRunScript, "Runs a script. The script is internal to the program and the program must be reco" +
        "mpiled.");
            this._btnRunScript.UseVisualStyleBackColor = false;
            this._btnRunScript.Click += new System.EventHandler(this._btnRunScript_Click);
            // 
            // _tmrScript
            // 
            this._tmrScript.Tick += new System.EventHandler(this._tmrScript_Tick);
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(959, 390);
            this.Controls.Add(this._btnRunScript);
            this.Controls.Add(this._btnExploreOutput);
            this.Controls.Add(this._btnDefaults);
            this.Controls.Add(this.label40);
            this.Controls.Add(this._chkCalculateEntropy);
            this.Controls.Add(this.label39);
            this.Controls.Add(this._btnRecordDir);
            this.Controls.Add(this._txtRecordDir);
            this.Controls.Add(this._chkRecord);
            this.Controls.Add(this.label33);
            this.Controls.Add(this._sSegP);
            this.Controls.Add(this.label38);
            this.Controls.Add(this._sSegA);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this._chkInformed);
            this.Controls.Add(this._btnLoad);
            this.Controls.Add(this._btnSave);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label34);
            this.Controls.Add(this._sRandomMethod);
            this.Controls.Add(this._sInformedRate);
            this.Controls.Add(this.label32);
            this.Controls.Add(this._sInformedAng);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this._lstInformed);
            this.Controls.Add(this.label29);
            this.Controls.Add(this._sRndAngle);
            this.Controls.Add(this.label26);
            this.Controls.Add(this._sRndInterval);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this._sDrawSegments);
            this.Controls.Add(this._sDrawTrails);
            this.Controls.Add(this.label25);
            this.Controls.Add(this._sSegUpdate);
            this.Controls.Add(this._sDrawUpdate);
            this.Controls.Add(this.label22);
            this.Controls.Add(this._sSegY);
            this.Controls.Add(this._sSegX);
            this.Controls.Add(this.label21);
            this.Controls.Add(this._chkWrap);
            this.Controls.Add(this.label20);
            this.Controls.Add(this._lstTrails);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this._chkReset);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnOk);
            this.Controls.Add(this._sOrientationMethod);
            this.Controls.Add(this._lstChange);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this._sMov);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label12);
            this.Controls.Add(this._sNum);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this._sAvoAng);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._sAvoDist);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this._sAlnAng);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._sAlnDist);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._sFov);
            this.Controls.Add(this.label11);
            this.Controls.Add(this._sTowAng);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._sTowDist);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmSettings";
            this.Opacity = 0.75D;
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(this._sTowDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sTowAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAlnAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAlnDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAvoAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sAvoDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sFov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sMov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sDrawUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sRndAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sRndInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sInformedAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sInformedRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sSegP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown _sTowDist;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown _sTowAng;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown _sAlnAng;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown _sAlnDist;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown _sAvoAng;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown _sAvoDist;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown _sFov;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown _sNum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown _sMov;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox _lstChange;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.CheckBox _chkReset;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox _lstTrails;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox _chkWrap;
        private System.Windows.Forms.NumericUpDown _sSegX;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown _sSegY;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown _sDrawUpdate;
        private System.Windows.Forms.NumericUpDown _sSegUpdate;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox _sDrawTrails;
        private System.Windows.Forms.CheckBox _sDrawSegments;
        private System.Windows.Forms.NumericUpDown _sRndAngle;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown _sRndInterval;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox _sOrientationMethod;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox _lstInformed;
        private System.Windows.Forms.NumericUpDown _sInformedAng;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.NumericUpDown _sInformedRate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox _sRandomMethod;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button _btnSave;
        private System.Windows.Forms.Button _btnLoad;
        private System.Windows.Forms.CheckBox _chkInformed;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.NumericUpDown _sSegA;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown _sSegP;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button _btnRecordDir;
        private System.Windows.Forms.CheckBox _chkRecord;
        private System.Windows.Forms.TextBox _txtRecordDir;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox _chkCalculateEntropy;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button _btnDefaults;
        private System.Windows.Forms.LinkLabel _btnExploreOutput;
        private System.Windows.Forms.Button _btnRunScript;
        private System.Windows.Forms.Timer _tmrScript;
    }
}