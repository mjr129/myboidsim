﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBoidSim
{
    public partial class FrmSettings : Form
    {
        private Constants constants;
        private object locker;
        BoidSet set;

        public FrmSettings()
        {
            InitializeComponent();

            Populate<BoidChangeMode>(_lstChange);
            Populate<TrailMode>(_lstTrails);
            Populate<OrientationMethod>(_sOrientationMethod);
            Populate<InformedMethod>(_lstInformed);
            Populate<RandomMethod>(_sRandomMethod);
        }

        private static void Populate<T>(ComboBox l)
             where T : struct
        {
            foreach (var v in Enum.GetValues(typeof(T)))
            {
                l.Items.Add(v);
            }
        }

        internal FrmSettings(Constants constants, object locker, BoidSet set)
            : this()
        {
            this.constants = constants;
            this.locker = locker;
            this.set = set;

            LoadConstants();
        }

        enum Conversion
        {
            None,
            Degrees,
            DistanceSquared,
        }

        private void LoadConstants()
        {
            LoadConstant(_sAlnAng, constants.AlignWithAngle, Conversion.Degrees);
            LoadConstant(_sAlnDist, constants.AlignWithZone, Conversion.DistanceSquared);
            _lstChange.SelectedIndex = (int)constants.BoidChangeMode;
            _sNum.Value = constants.NumberOfBoids;
            LoadConstant(_sMov, constants.Speed, Conversion.None);
            LoadConstant(_sAvoAng, constants.TurnAwayFromAngle, Conversion.Degrees);
            LoadConstant(_sAvoDist, constants.TurnAwayFromZone, Conversion.DistanceSquared);
            LoadConstant(_sTowAng, constants.TurnTowardsAngle, Conversion.Degrees);
            LoadConstant(_sTowDist, constants.TurnTowardsZone, Conversion.DistanceSquared);
            LoadConstant(_sFov, constants.ViewAngle, Conversion.Degrees);
            LoadConstant(_sRndAngle, constants.RandomiseAngle, Conversion.Degrees);
            _sRndInterval.Value = constants.RandomiseInterval;
            _lstTrails.SelectedIndex = (int)constants.TrailMode;
            _chkWrap.Checked = constants.WrapScreen;
            _sDrawUpdate.Value = constants.DrawUpdate;
            _sDrawTrails.Checked = constants.DrawTrails;
            _sDrawSegments.Checked = constants.DrawSegments;
            _sOrientationMethod.SelectedIndex = (int)constants.OrientationMethod;
            _lstInformed.SelectedIndex = (int)constants.InformedMethod;
            LoadConstant(_sInformedAng, constants.InformedAngle, Conversion.Degrees);
            _sInformedRate.Value = constants.InformedRate;
            _sRandomMethod.SelectedIndex = (int)constants.RandomMethod;
            _chkInformed.Checked = constants.SplitInformation;

            _sSegX.Value = constants.SegmentsSpacialX;
            _sSegY.Value = constants.SegmentsSpacialY;
            _sSegA.Value = constants.SegmentsAngleCount;
            LoadConstant(_sSegP, constants.SegmentsTargetProximity, Conversion.DistanceSquared);
            _sSegUpdate.Value = constants.SegmentUpdate;
            _chkCalculateEntropy.Checked = constants.MonitorEntropy;
            _chkRecord.Checked = constants.RecordSegments;
            _txtRecordDir.Text = constants.RecordSegmentsDir;
        }

        private void SaveConstants()
        {
            constants.AlignWithAngle = SaveConstant(_sAlnAng, Conversion.Degrees);
            constants.AlignWithZone = SaveConstant(_sAlnDist, Conversion.DistanceSquared);
            constants.BoidChangeMode = (BoidChangeMode)_lstChange.SelectedIndex;
            constants.NumberOfBoids = (int)_sNum.Value;
            constants.Speed = SaveConstant(_sMov, Conversion.None);
            constants.TurnAwayFromAngle = SaveConstant(_sAvoAng, Conversion.Degrees);
            constants.TurnAwayFromZone = SaveConstant(_sAvoDist, Conversion.DistanceSquared);
            constants.TurnTowardsAngle = SaveConstant(_sTowAng, Conversion.Degrees);
            constants.TurnTowardsZone = SaveConstant(_sTowDist, Conversion.DistanceSquared);
            constants.ViewAngle = SaveConstant(_sFov, Conversion.Degrees);
            constants.Reset = _chkReset.Checked;
            constants.TrailMode = (TrailMode)_lstTrails.SelectedIndex;
            constants.WrapScreen = _chkWrap.Checked;
            constants.DrawUpdate = (int)_sDrawUpdate.Value;
            constants.DrawTrails = _sDrawTrails.Checked;
            constants.DrawSegments = _sDrawSegments.Checked;
            constants.RandomiseAngle = SaveConstant(_sRndAngle, Conversion.Degrees);
            constants.RandomiseInterval = (int)_sRndInterval.Value;
            constants.OrientationMethod = (OrientationMethod)_sOrientationMethod.SelectedIndex;
            constants.InformedMethod = (InformedMethod)_lstInformed.SelectedIndex;
            constants.InformedAngle = SaveConstant(_sInformedAng, Conversion.Degrees);
            constants.InformedRate = (int)_sInformedRate.Value;
            constants.RandomMethod = (RandomMethod)_sRandomMethod.SelectedIndex;
            constants.SplitInformation = _chkInformed.Checked;

            constants.SegmentsSpacialX = (int)_sSegX.Value;
            constants.SegmentsSpacialY = (int)_sSegY.Value;
            constants.SegmentsAngleCount = (int)_sSegA.Value;
            constants.SegmentsTargetProximity = SaveConstant(_sSegP, Conversion.DistanceSquared);
            constants.SegmentUpdate = (int)_sSegUpdate.Value;
            constants.MonitorEntropy = _chkCalculateEntropy.Checked;
            constants.RecordSegments = _chkRecord.Checked;
            constants.RecordSegmentsDir = _txtRecordDir.Text;
        }

        private double SaveConstant(NumericUpDown c, Conversion m)
        {
            double v = (double)c.Value;

            switch (m)
            {
                case Conversion.None:
                    break;

                case Conversion.Degrees:
                    v = (v / 180) * Math.PI;
                    break;

                case Conversion.DistanceSquared:
                    v = v * v;
                    break;

                default:
                    throw new ArgumentException("Invalid conversion: " + m.ToString(), "m");
            }

            return v;
        }

        private void LoadConstant(NumericUpDown c, double v, Conversion m)
        {
            switch (m)
            {
                case Conversion.None:
                    break;

                case Conversion.Degrees:
                    v = (v / Math.PI) * 180;
                    break;

                case Conversion.DistanceSquared:
                    v = Math.Sqrt(v);
                    break;

                default:
                    throw new ArgumentException("Invalid conversion: " + m.ToString(), "m");
            }

            c.Value = (decimal)v;
        }

        private void _btnOk_Click(object sender, EventArgs e)
        {
            lock (locker)
            {
                SaveConstants();
            }
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            LoadConstants();
        }

        private void _btnSave_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "XML|*.xml";

                if (sfd.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    constants.Save(sfd.FileName);
                }
            }
        }

        private void _btnLoad_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "XML|*.xml";

                if (ofd.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    var c = Constants.Load(ofd.FileName);

                    constants.Assign(c);

                    LoadConstants();
                }
            }
        }

        private void _lstInformed_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void _btnRecordDir_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.OverwritePrompt = false;
                sfd.FileName = Path.Combine(_txtRecordDir.Text, "Target Directory");
                sfd.Filter = "Directories|*.*";
                sfd.Title = "Select Directory";

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    _txtRecordDir.Text = Path.GetDirectoryName(sfd.FileName);
                }
            }
        }

        private void _chkMonitorEntropy_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void _btnDefaults_Click(object sender, EventArgs e)
        {
            constants.Assign(new Constants());
            LoadConstants();
        }

        private void _btnExploreOutput_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("Explorer.exe", "/select,\"" + _txtRecordDir.Text + "\"");
        }


        bool scriptReady = false;
        int scriptIteration = 0;

        private void _btnRunScript_Click(object sender, EventArgs e)
        {
            Enabled = false;

            lock (locker)
            {
                constants.Reset = true;
                constants.InformedAngle = 0.0;
                set.SegmentFilePrefix = scriptIteration.ToString() + "-" + ((constants.InformedAngle / Math.PI) * 180) + "--";
                LoadConstants();
            }

            _tmrScript.Enabled = true;
        }

        private void _tmrScript_Tick(object sender, EventArgs e)
        {
            lock (locker)
            {
                _btnRunScript.Text = scriptIteration + "/" + set.Age.ToString();
                Text = "SCRIPT: " + _btnRunScript.Text;

                if (set.Age < 3000)
                {
                    scriptReady = true;
                }
                else if (scriptReady)
                {
                    constants.Reset = true;

                    if (constants.InformedAngle > ((Math.PI / 180) * 20))
                    {
                        constants.InformedAngle = 0;
                        scriptIteration = 0;
                    }
                    else
                    {
                        constants.InformedAngle += (Math.PI / 180) * 0.05;
                    }
                    set.SegmentFilePrefix = scriptIteration.ToString() + "-" + ((constants.InformedAngle / Math.PI) * 180) + "--";
                    LoadConstants();
                }
            }
        }
    }
}
