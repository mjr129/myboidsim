﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    class MiBoidTypes
    {
        InformedStatus[] statuses;
        int[] counts;
        int[,] partnerIndexes;
        int[] typeIndexes;
        int[,] actualIndexes;

        public MiBoidTypes(string fn)
        {
            int[,] csv = MiGeneral.ReadCsv(fn);
            counts = new int[4];
            statuses = new InformedStatus[csv.GetLength(0)];
            actualIndexes = new int[3, csv.GetLength(0)];
            typeIndexes = new int[csv.GetLength(0)];

            for (int n = 0; n < csv.GetLength(0); n++)
            {
                int informedStatus = csv[n, 1];
                statuses[n] = (InformedStatus)informedStatus;
                counts[0]++;
                typeIndexes[n] = counts[informedStatus + 1];
                counts[informedStatus + 1]++;
                actualIndexes[informedStatus, typeIndexes[n]] = n;
            }

            partnerIndexes = new int[csv.GetLength(0), 3];

            for (int x = 0; x < csv.GetLength(0); x++)
            {
                for (int y = 0; y < csv.GetLength(0); y++)
                {
                    if (typeIndexes[x] == typeIndexes[y])
                    {
                        partnerIndexes[x, (int)statuses[y]] = y;
                    }
                }
            }
        }

        public int GetActualIndex(InformedStatus informedStatus, int typeIndex)
        {
            if (informedStatus == InformedStatus.All)
            {
                return typeIndex;
            }

            return actualIndexes[(int)informedStatus, typeIndex];
        }

        public InformedStatus GetType(int index)
        {
            return statuses[index];
        }

        public int GetPartnerIndex(int index, InformedStatus partnerType)
        {
            return partnerIndexes[index, (int)partnerType];
        }

        public int GetCount(InformedStatus informedStatus)
        {
            return GetCount((int)informedStatus);
        }

        private int GetCount(int informedStatus)
        {
            return counts[informedStatus + 1];
        }
    }
}
