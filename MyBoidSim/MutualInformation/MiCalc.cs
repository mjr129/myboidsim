﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    class MiCalc
    {
        public MiBoidTypes boidTypes;
        public MiSegmentCounts segmentCounts;
        MiSegment[] segments;
        public int MaxAge;

        public MiCalc(string dir)
        {
            boidTypes = new MiBoidTypes(Path.Combine(dir, "BoidTypes.csv"));
            segmentCounts = new MiSegmentCounts(Path.Combine(dir, "SegmentTypes.csv"));
            segments = new MiSegment[3];

            segments[0] = new MiSegment(Path.Combine(dir, "BoidsSpacial.csv"));
            segments[1] = new MiSegment(Path.Combine(dir, "BoidsAngular.csv"));
            segments[2] = new MiSegment(Path.Combine(dir, "BoidsProximity.csv"));

            MaxAge = segments[0].GetAgeCount();
        }

        public MiSegment GetSegment(SegmentType segmentType)
        {
            return segments[(int)segmentType];
        }
    }
}
