﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    class MiGeneral
    {
        public static int[,] ReadCsv(string fn)
        {
            List<int[]> list = new List<int[]>();

            using (StreamReader sr = new StreamReader(fn))
            {
                sr.ReadLine(); // skip titles

                while (!sr.EndOfStream)
                {
                    string[] el = sr.ReadLine().Split(",".ToCharArray());

                    if (el.Length == 1 && el[0].Length == 0)
                    {
                        continue;
                    }

                    int[] i = new int[el.Length];

                    for (int n = 0; n < el.Length; n++)
                    {
                        i[n] = int.Parse(el[n]);
                    }

                    list.Add(i);
                }
            }

            int[,] r = new int[list.Count, list[0].Length];

            for (int row = 0; row < list.Count; row++)
            {
                for (int col = 0; col < list[row].Length; col++)
                {
                    r[row, col] = list[row][col];
                }
            }

            return r;
        }

        public static double[] ToProbabilities(int[] counts, int boidCount)
        {
            double[] result = new double[counts.Length];

            for (int n = 0; n < counts.Length; n++)
            {
                result[n] = (double)counts[n] / boidCount;
            }

            return result;
        }

        public static double[,] ToProbabilities(int[,] counts, int boidCount)
        {
            double[,] result = new double[counts.GetLength(0), counts.GetLength(1)];

            for (int n = 0; n < counts.GetLength(0); n++)
            {
                for (int m = 0; m < counts.GetLength(1); m++)
                {
                    result[n, m] = (double)counts[n, m] / boidCount;
                }
            }

            return result;
        }
    }
}
