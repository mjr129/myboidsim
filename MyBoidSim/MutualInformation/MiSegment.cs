﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    class MiSegment
    {
        int[,] data;
        int ageCount;
        int boidCount;

        public MiSegment(string fn)
        {
            data = MiGeneral.ReadCsv(fn);
            ageCount = data.GetLength(0);
            boidCount = data.GetLength(1);
        }

        public int GetBoidCount()
        {
            return boidCount;
        }

        public int GetAgeCount()
        {
            return ageCount;
        }

        public int GetPosition(int age, int boidIndex)
        {
            return data[age, boidIndex];
        }
    }
}
