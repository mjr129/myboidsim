﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    enum SegmentType
    {
        Spacial,
        Angular,
        Proximity
    }

    class MiSegmentCounts
    {
        int[] counts;

        public MiSegmentCounts(string fn)
        {
            int[,] csv = MiGeneral.ReadCsv(fn);
            counts = new int[csv.GetLength(0)];

            for (int n = 0; n < csv.GetLength(0); n++)
            {
                counts[n] = csv[n, 1];
            }
        }

        public int GetCount(SegmentType type)
        {
            return counts[(int)type];
        }
    }
}
