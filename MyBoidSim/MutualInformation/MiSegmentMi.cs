﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim.MutualInformation
{
    class MiSegmentMi
    {
        public static double[] CalculateProbabilities(MiCalc calc, InformedStatus isA, SegmentType stA, int minAge, int maxAge)
        {
            int xNum = calc.boidTypes.GetCount(isA);
            var segA = calc.GetSegment(stA);
            int stcA = calc.segmentCounts.GetCount(stA);
            int[] counts = new int[stcA];
            int t = 0;
            maxAge = Math.Min(maxAge, segA.GetAgeCount());

            for (int age = minAge; age <= maxAge; age++)
            {
                for (int bi = 0; bi < xNum; bi++)
                {
                    int boidA = calc.boidTypes.GetActualIndex(isA, bi);

                    int boidAseg = segA.GetPosition(age, boidA);

                    counts[boidAseg]++;
                    t++;
                }
            }

            return MiGeneral.ToProbabilities(counts, t);
        }

        public static double[,] CalculateJointProbabilities(MiCalc calc, InformedStatus isA, InformedStatus isB, SegmentType stA, SegmentType stB, int minAge, int maxAge, int relAgeB)
        {
            return CalculateJointProbabilities(calc, isA, isB, stA, stB, minAge, maxAge, relAgeB, false);
        }

        public static double[,] CalculateJointProbabilities(MiCalc calc, InformedStatus isA, InformedStatus isB, SegmentType stA, SegmentType stB, int minAge, int maxAge, int relAgeB, bool inverseB)
        {
            int aNum = calc.boidTypes.GetCount(isA);
            int bNum = calc.boidTypes.GetCount(isB);
            var segA = calc.GetSegment(stA);
            var segB = calc.GetSegment(stB);
            int stcA = calc.segmentCounts.GetCount(stA);
            int stcB = calc.segmentCounts.GetCount(stB);
            int[,] counts = new int[stcA, stcB];
            int t = 0;
            maxAge = Math.Min(maxAge, segA.GetAgeCount() - 1);

            for (int age = minAge; age <= maxAge; age++)
            {
                for (int bi = 0; bi < aNum; bi++)
                {
                    int boidA = calc.boidTypes.GetActualIndex(isA, bi);
                    int boidB = calc.boidTypes.GetActualIndex(isB, bi);

                    if (inverseB)
                    {
                        boidB = bNum - boidB - 1;
                    }

                    int boidAseg = segA.GetPosition(age, boidA);
                    int boidBseg = segB.GetPosition(age + relAgeB, boidB);

                    counts[boidAseg, boidBseg]++;
                    t++;
                }
            }

            return MiGeneral.ToProbabilities(counts, t);
        }
    }
}
