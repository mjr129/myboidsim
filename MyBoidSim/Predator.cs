﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    class Predator
    {
        public Vector Location;
        public BoidSet Owner;

        public Predator(BoidSet owner, int p1, int p2)
        {
            Location = new Vector(p1, p2);
            Owner = owner;
        }

        internal void Process()
        {
            if (Owner.Constants.InformedMethod == InformedMethod.FloatingPoint)
            {
                int a = Owner.Age;
                double speed = 0.005;
                double r = Math.Min(Owner.maxX, Owner.maxY) * 0.33;
                //double i = Math.Cos(a * (speed / 2));
                //Location.Y = Math.Cos(a * speed) / Math.PI * Owner.maxY * i + Owner.maxY / 2;
                //Location.X = Math.Sin(a * speed) / Math.PI * Owner.maxX * i + Owner.maxX / 2;
                Location.Y = Math.Cos(a * speed) * r + Owner.maxY / 2;
                Location.X = Math.Sin(a * speed) * r + Owner.maxX / 2;
            }
            else
            {
                Location.Y = Owner.maxY / 2;
                Location.X = Owner.maxX / 2;
            }
        }
    }
}
