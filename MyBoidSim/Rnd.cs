﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    static class Rnd
    {
        public static Random Random = new Random();

        public static bool Chance(double chance)
        {
            return Random.NextDouble() < chance;
        }
    }
}
