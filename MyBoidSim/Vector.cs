﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBoidSim
{
    struct VectorInt
    {
        public int X;
        public int Y;

        public VectorInt(int x, int y)
        {
            X = x;
            Y = y;
        }

        internal int DistanceSquared(int x, int y)
        {
            int dx = x - X;
            int dy = y - Y;

            return dx * dx + dy * dy;
        }

        internal Vector ToDouble()
        {
            return new Vector(X, Y);
        }

        public override string ToString()
        {
            return X.ToString() + ", " + Y.ToString();
        }

        public static VectorInt operator +(VectorInt l, VectorInt r)
        {
            return new VectorInt(l.X + r.X, l.Y + r.Y);
        }

        public static VectorInt operator -(VectorInt l, VectorInt r)
        {
            return new VectorInt(l.X - r.X, l.Y - r.Y);
        }
    }

    struct Vector
    {
        public double X;
        public double Y;

        public Vector(double a)
        {
            X = Math.Cos(a);
            Y = Math.Sin(a);
        }

        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double DistanceSquared(Vector to)
        {
            double dx = to.X - X;
            double dy = to.Y - Y;

            return dx * dx + dy * dy;
        }

        public double Distance(Vector to)
        {
            double dx = to.X - X;
            double dy = to.Y - Y;

            return Math.Sqrt(dx * dx + dy * dy);
        }

        public VectorInt ToInt()
        {
            return new VectorInt((int)X, (int)Y);
        }

        public double AngleTo(VectorInt point)
        {
            return AngleTo(point.ToDouble());
        }

        public double AngleTo(Vector to)
        {
            double dx = to.X - X;
            double dy = to.Y - Y;

            return Math.Atan2(dy, dx);
        }

        public override string ToString()
        {
            return X.ToString() + ", " + Y.ToString();
        }

        internal double DistanceSquared(VectorInt to)
        {
            double dx = to.X - X;
            double dy = to.Y - Y;

            return dx * dx + dy * dy;
        }

        public static Vector operator -(Vector l, Vector r)
        {
            return new Vector(l.X - r.X, l.Y - r.Y);
        }

        public static Vector operator -(Vector l, VectorInt r)
        {
            return new Vector(l.X - r.X, l.Y - r.Y);
        }

        public static Vector operator +(Vector l, Vector r)
        {
            return new Vector(l.X + r.X, l.Y + r.Y);
        }

        public static Vector operator /(Vector l, int r)
        {
            return new Vector(l.X / (double)r, l.Y / (double)r);
        }

        internal double GetAngle()
        {
            return Math.Atan2(Y, X);
        }
    }
}
