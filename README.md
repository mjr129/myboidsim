My Boid Sim
===========

A customisable boid simulator GUI that uses Shannon Entropy as a measure of collectiveness.


Installation
------------

Via source code only: Open the SLN file in Visual Studio and press run.

Meta
----

```ini
host=bitbucket
url=https://bitbucket.org/mjr129/myboidsim
author=Martin Rusilowicz
language=C#
date=2015
type=application
```